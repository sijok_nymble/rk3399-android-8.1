/**
 * @file   
 * @author 
 * @date   
 * @brief  A kernel module for controlling a gpio (or any signal) that is connected to
 * a external mcu reset pin. It is threaded in order that it can three modes reset ,off and on.
 * The sysfs entry appears at /sys/ngpio/mod*
 * @see 
*/

#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/gpio.h>       // Required for the GPIO functions
#include <linux/kobject.h>    // Using kobjects for the sysfs bindings
#include <linux/kthread.h>    // Using kthreads for the flashing functionality
#include <linux/delay.h>      // Using this header for the msleep() function

MODULE_LICENSE("GPL");
MODULE_AUTHOR("sijok");
MODULE_DESCRIPTION("GPIO toggle driver for IND mcu and CB mcu reset ");
MODULE_VERSION("0.1");

static unsigned int gpio7 = 32;           
module_param(gpio7, uint, S_IRUGO);       ///< Param desc. S_IRUGO can be read/not changed
MODULE_PARM_DESC(gpio7, "GPIO7");     ///< parameter description

static unsigned int gpio11 = 33;           
module_param(gpio11, uint, S_IRUGO);       ///< Param desc. S_IRUGO can be read/not changed
MODULE_PARM_DESC(gpio11, "GPIO11");     ///< parameter description

static unsigned int gpio13 = 35;           
module_param(gpio13, uint, S_IRUGO);       ///< Param desc. S_IRUGO can be read/not changed
MODULE_PARM_DESC(gpio13, "GPIO13");     ///< parameter description

static unsigned int gpio15 = 36;           
module_param(gpio15, uint, S_IRUGO);       ///< Param desc. S_IRUGO can be read/not changed
MODULE_PARM_DESC(gpio15, "GPIO15");     ///< parameter description

static unsigned int gpio16 = 54;           
module_param(gpio16, uint, S_IRUGO);       ///< Param desc. S_IRUGO can be read/not changed
MODULE_PARM_DESC(gpio16, "GPIO16");     ///< parameter description

static unsigned int gpio18 = 55;           
module_param(gpio18, uint, S_IRUGO);       ///< Param desc. S_IRUGO can be read/not changed
MODULE_PARM_DESC(gpio18, "GPIO18");     ///< parameter description

static unsigned int gpio22 = 56;           
module_param(gpio22, uint, S_IRUGO);       ///< Param desc. S_IRUGO can be read/not changed
MODULE_PARM_DESC(gpio22, "GPIO22");     ///< parameter description

static unsigned int gpio26 = 149;           
module_param(gpio26, uint, S_IRUGO);       ///< Param desc. S_IRUGO can be read/not changed
MODULE_PARM_DESC(gpio26, "GPIO26");     ///< parameter description

static unsigned int gpio37 = 124;           
module_param(gpio37, uint, S_IRUGO);       ///< Param desc. S_IRUGO can be read/not changed
MODULE_PARM_DESC(gpio37, "GPIO37");     ///< parameter description

static unsigned int gpio38 = 125;           
module_param(gpio38, uint, S_IRUGO);       ///< Param desc. S_IRUGO can be read/not changed
MODULE_PARM_DESC(gpio38, "GPIO38");     ///< parameter description

static unsigned int gpio40 = 126;           
module_param(gpio40, uint, S_IRUGO);       ///< Param desc. S_IRUGO can be read/not changed
MODULE_PARM_DESC(gpio40, "GPIO40");     ///< parameter description
      
static char gpioName7[7] = "ledXXX";
static char gpioName11[7] = "ledXXX"; 
static char gpioName13[7] = "ledXXX";
static char gpioName15[7] = "ledXXX"; 
static char gpioName16[7] = "ledXXX";
static char gpioName18[7] = "ledXXX"; 
static char gpioName22[7] = "ledXXX";
static char gpioName26[7] = "ledXXX"; 
static char gpioName37[7] = "ledXXX"; 
static char gpioName38[7] = "ledXXX"; 
static char gpioName40[7] = "ledXXX"; 
static bool gpioOn = 0;                      
enum modes { OFF, ON, RESET ,NSTATE};                         
static enum modes mode7 = OFF;             
static enum modes mode11 = OFF;
static enum modes mode13 = OFF;             
static enum modes mode15 = OFF;
static enum modes mode16 = OFF;             
static enum modes mode18 = OFF;
static enum modes mode22 = OFF;             
static enum modes mode26 = OFF;
static enum modes mode37 = OFF;
static enum modes mode38 = OFF;
static enum modes mode40 = OFF;


/** @brief A callback function to display the LED mode
 *  @param kobj represents a kernel object device that appears in the sysfs filesystem
 *  @param attr the pointer to the kobj_attribute struct
 *  @param buf the buffer to which to write the number of presses
 *  @return return the number of characters of the mode string successfully displayed
 */

static ssize_t mode_show7(struct kobject *kobj, struct kobj_attribute *attr, char *buf){
   switch(mode7){
      case OFF:   return sprintf(buf, "off\n");       // Display the state -- simplistic approach
      case ON:    return sprintf(buf, "on\n");
      case RESET: return sprintf(buf, "reset\n");
      default:    return sprintf(buf, "LKM Error\n"); // Cannot get here
   }
}

/** @brief A callback function to store the  mode using the enum above */
static ssize_t mode_store7(struct kobject *kobj, struct kobj_attribute *attr, const char *buf, size_t count){
   // the count-1 is important as otherwise the \n is used in the comparison
   if (strncmp(buf,"on",count-1)==0) { mode7 = ON; }   // strncmp() compare with fixed number chars
   else if (strncmp(buf,"off",count-1)==0) { mode7 = OFF; }
   else if (strncmp(buf,"reset",count-1)==0) { mode7 = RESET; }
   return count;
}

/*
*
*/

static ssize_t mode_show11(struct kobject *kobj, struct kobj_attribute *attr, char *buf){
   switch(mode11){
      case OFF:   return sprintf(buf, "off\n");       // Display the state -- simplistic approach
      case ON:    return sprintf(buf, "on\n");
      case RESET: return sprintf(buf, "reset\n");
      default:    return sprintf(buf, "LKM Error\n"); // Cannot get here
   }
}

/** @brief A callback function to store the  mode using the enum above */
static ssize_t mode_store11(struct kobject *kobj, struct kobj_attribute *attr, const char *buf, size_t count){
   // the count-1 is important as otherwise the \n is used in the comparison
   if (strncmp(buf,"on",count-1)==0) { mode11 = ON; }   // strncmp() compare with fixed number chars
   else if (strncmp(buf,"off",count-1)==0) { mode11 = OFF; }
   else if (strncmp(buf,"reset",count-1)==0) { mode11 = RESET; }
   return count;
}

/*
*
*/

static ssize_t mode_show13(struct kobject *kobj, struct kobj_attribute *attr, char *buf){
   switch(mode13){
      case OFF:   return sprintf(buf, "off\n");       // Display the state -- simplistic approach
      case ON:    return sprintf(buf, "on\n");
      case RESET: return sprintf(buf, "reset\n");
      default:    return sprintf(buf, "LKM Error\n"); // Cannot get here
   }
}

/** @brief A callback function to store the  mode using the enum above */
static ssize_t mode_store13(struct kobject *kobj, struct kobj_attribute *attr, const char *buf, size_t count){
   // the count-1 is important as otherwise the \n is used in the comparison
   if (strncmp(buf,"on",count-1)==0) { mode13 = ON; }   // strncmp() compare with fixed number chars
   else if (strncmp(buf,"off",count-1)==0) { mode13 = OFF; }
   else if (strncmp(buf,"reset",count-1)==0) { mode13 = RESET; }
   return count;
}

/*
*
*/

static ssize_t mode_show15(struct kobject *kobj, struct kobj_attribute *attr, char *buf){
   switch(mode15){
      case OFF:   return sprintf(buf, "off\n");       // Display the state -- simplistic approach
      case ON:    return sprintf(buf, "on\n");
      case RESET: return sprintf(buf, "reset\n");
      default:    return sprintf(buf, "LKM Error\n"); // Cannot get here
   }
}

/** @brief A callback function to store the  mode using the enum above */
static ssize_t mode_store15(struct kobject *kobj, struct kobj_attribute *attr, const char *buf, size_t count){
   // the count-1 is important as otherwise the \n is used in the comparison
   if (strncmp(buf,"on",count-1)==0) { mode15 = ON; }   // strncmp() compare with fixed number chars
   else if (strncmp(buf,"off",count-1)==0) { mode15 = OFF; }
   else if (strncmp(buf,"reset",count-1)==0) { mode15 = RESET; }
   return count;
}

/*
*
*/

static ssize_t mode_show16(struct kobject *kobj, struct kobj_attribute *attr, char *buf){
   switch(mode16){
      case OFF:   return sprintf(buf, "off\n");       // Display the state -- simplistic approach
      case ON:    return sprintf(buf, "on\n");
      case RESET: return sprintf(buf, "reset\n");
      default:    return sprintf(buf, "LKM Error\n"); // Cannot get here
   }
}

/** @brief A callback function to store the  mode using the enum above */
static ssize_t mode_store16(struct kobject *kobj, struct kobj_attribute *attr, const char *buf, size_t count){
   // the count-1 is important as otherwise the \n is used in the comparison
   if (strncmp(buf,"on",count-1)==0) { mode16 = ON; }   // strncmp() compare with fixed number chars
   else if (strncmp(buf,"off",count-1)==0) { mode16 = OFF; }
   else if (strncmp(buf,"reset",count-1)==0) { mode16 = RESET; }
   return count;
}

/*
*
*/

static ssize_t mode_show18(struct kobject *kobj, struct kobj_attribute *attr, char *buf){
   switch(mode18){
      case OFF:   return sprintf(buf, "off\n");       // Display the state -- simplistic approach
      case ON:    return sprintf(buf, "on\n");
      case RESET: return sprintf(buf, "reset\n");
      default:    return sprintf(buf, "LKM Error\n"); // Cannot get here
   }
}

/** @brief A callback function to store the  mode using the enum above */
static ssize_t mode_store18(struct kobject *kobj, struct kobj_attribute *attr, const char *buf, size_t count){
   // the count-1 is important as otherwise the \n is used in the comparison
   if (strncmp(buf,"on",count-1)==0) { mode18 = ON; }   // strncmp() compare with fixed number chars
   else if (strncmp(buf,"off",count-1)==0) { mode18 = OFF; }
   else if (strncmp(buf,"reset",count-1)==0) { mode18 = RESET; }
   return count;
}

/*
*
*/

static ssize_t mode_show22(struct kobject *kobj, struct kobj_attribute *attr, char *buf){
   switch(mode22){
      case OFF:   return sprintf(buf, "off\n");       // Display the state -- simplistic approach
      case ON:    return sprintf(buf, "on\n");
      case RESET: return sprintf(buf, "reset\n");
      default:    return sprintf(buf, "LKM Error\n"); // Cannot get here
   }
}

/** @brief A callback function to store the  mode using the enum above */
static ssize_t mode_store22(struct kobject *kobj, struct kobj_attribute *attr, const char *buf, size_t count){
   // the count-1 is important as otherwise the \n is used in the comparison
   if (strncmp(buf,"on",count-1)==0) { mode22 = ON; }   // strncmp() compare with fixed number chars
   else if (strncmp(buf,"off",count-1)==0) { mode22 = OFF; }
   else if (strncmp(buf,"reset",count-1)==0) { mode22 = RESET; }
   return count;
}

/*
*
*/

static ssize_t mode_show26(struct kobject *kobj, struct kobj_attribute *attr, char *buf){
   switch(mode26){
      case OFF:   return sprintf(buf, "off\n");       // Display the state -- simplistic approach
      case ON:    return sprintf(buf, "on\n");
      case RESET: return sprintf(buf, "reset\n");
      default:    return sprintf(buf, "LKM Error\n"); // Cannot get here
   }
}

/** @brief A callback function to store the  mode using the enum above */
static ssize_t mode_store26(struct kobject *kobj, struct kobj_attribute *attr, const char *buf, size_t count){
   // the count-1 is important as otherwise the \n is used in the comparison
   if (strncmp(buf,"on",count-1)==0) { mode26 = ON; }   // strncmp() compare with fixed number chars
   else if (strncmp(buf,"off",count-1)==0) { mode26 = OFF; }
   else if (strncmp(buf,"reset",count-1)==0) { mode26 = RESET; }
   return count;
}

/*
*
*/

static ssize_t mode_show37(struct kobject *kobj, struct kobj_attribute *attr, char *buf){
   switch(mode37){
      case OFF:   return sprintf(buf, "off\n");       // Display the state -- simplistic approach
      case ON:    return sprintf(buf, "on\n");
      case RESET: return sprintf(buf, "reset\n");
      default:    return sprintf(buf, "LKM Error\n"); // Cannot get here
   }
}

/** @brief A callback function to store the  mode using the enum above */
static ssize_t mode_store37(struct kobject *kobj, struct kobj_attribute *attr, const char *buf, size_t count){
   // the count-1 is important as otherwise the \n is used in the comparison
   if (strncmp(buf,"on",count-1)==0) { mode37 = ON; }   // strncmp() compare with fixed number chars
   else if (strncmp(buf,"off",count-1)==0) { mode37 = OFF; }
   else if (strncmp(buf,"reset",count-1)==0) { mode37 = RESET; }
   return count;
}

/*
*
*/

static ssize_t mode_show38(struct kobject *kobj, struct kobj_attribute *attr, char *buf){
   switch(mode38){
      case OFF:   return sprintf(buf, "off\n");       // Display the state -- simplistic approach
      case ON:    return sprintf(buf, "on\n");
      case RESET: return sprintf(buf, "reset\n");
      default:    return sprintf(buf, "LKM Error\n"); // Cannot get here
   }
}

/** @brief A callback function to store the  mode using the enum above */
static ssize_t mode_store38(struct kobject *kobj, struct kobj_attribute *attr, const char *buf, size_t count){
   // the count-1 is important as otherwise the \n is used in the comparison
   if (strncmp(buf,"on",count-1)==0) { mode38 = ON; }   // strncmp() compare with fixed number chars
   else if (strncmp(buf,"off",count-1)==0) { mode38 = OFF; }
   else if (strncmp(buf,"reset",count-1)==0) { mode38 = RESET; }
   return count;
}

/*
*
*/

static ssize_t mode_show40(struct kobject *kobj, struct kobj_attribute *attr, char *buf){
   switch(mode40){
      case OFF:   return sprintf(buf, "off\n");       // Display the state -- simplistic approach
      case ON:    return sprintf(buf, "on\n");
      case RESET: return sprintf(buf, "reset\n");
      default:    return sprintf(buf, "LKM Error\n"); // Cannot get here
   }
}

/** @brief A callback function to store the  mode using the enum above */
static ssize_t mode_store40(struct kobject *kobj, struct kobj_attribute *attr, const char *buf, size_t count){
   // the count-1 is important as otherwise the \n is used in the comparison
   if (strncmp(buf,"on",count-1)==0) { mode40 = ON; }   // strncmp() compare with fixed number chars
   else if (strncmp(buf,"off",count-1)==0) { mode40 = OFF; }
   else if (strncmp(buf,"reset",count-1)==0) { mode40 = RESET; }
   return count;
}



/* warning! need write-all permission so overriding check */ 
#undef VERIFY_OCTAL_PERMISSIONS
#define VERIFY_OCTAL_PERMISSIONS(perms) (perms)



/** Use these helper macros to define the name and access levels of the kobj_attributes
 *  The kobj_attribute has an attribute attr (name and mode), show and store function pointers
 *  The period variable is associated with the blinkPeriod variable and it is to be exposed
 *  with mode 0666 using the period_show and period_store functions above
 */

/* warning! need write-all permission so overriding check */ 
#undef VERIFY_OCTAL_PERMISSIONS
#define VERIFY_OCTAL_PERMISSIONS(perms) (perms)

static struct kobj_attribute g7_attr = __ATTR(mode7, 0777, mode_show7, mode_store7);

#undef VERIFY_OCTAL_PERMISSIONS
#define VERIFY_OCTAL_PERMISSIONS(perms) (perms)
static struct kobj_attribute g11_attr = __ATTR(mode11, 0777, mode_show11, mode_store11);

#undef VERIFY_OCTAL_PERMISSIONS
#define VERIFY_OCTAL_PERMISSIONS(perms) (perms)
static struct kobj_attribute g13_attr = __ATTR(mode13, 0777, mode_show13, mode_store13);

#undef VERIFY_OCTAL_PERMISSIONS
#define VERIFY_OCTAL_PERMISSIONS(perms) (perms)
static struct kobj_attribute g15_attr = __ATTR(mode15, 0777, mode_show15, mode_store15);

#undef VERIFY_OCTAL_PERMISSIONS
#define VERIFY_OCTAL_PERMISSIONS(perms) (perms)
static struct kobj_attribute g16_attr = __ATTR(mode16, 0777, mode_show16, mode_store16);

#undef VERIFY_OCTAL_PERMISSIONS
#define VERIFY_OCTAL_PERMISSIONS(perms) (perms)
static struct kobj_attribute g18_attr = __ATTR(mode18, 0777, mode_show18, mode_store18);

#undef VERIFY_OCTAL_PERMISSIONS
#define VERIFY_OCTAL_PERMISSIONS(perms) (perms)
static struct kobj_attribute g22_attr = __ATTR(mode22, 0777, mode_show22, mode_store22);

#undef VERIFY_OCTAL_PERMISSIONS
#define VERIFY_OCTAL_PERMISSIONS(perms) (perms)
static struct kobj_attribute g26_attr = __ATTR(mode26, 0777, mode_show26, mode_store26);

#undef VERIFY_OCTAL_PERMISSIONS
#define VERIFY_OCTAL_PERMISSIONS(perms) (perms)
static struct kobj_attribute g37_attr = __ATTR(mode37, 0777, mode_show37, mode_store37);

#undef VERIFY_OCTAL_PERMISSIONS
#define VERIFY_OCTAL_PERMISSIONS(perms) (perms)
static struct kobj_attribute g38_attr = __ATTR(mode38, 0777, mode_show38, mode_store38);

#undef VERIFY_OCTAL_PERMISSIONS
#define VERIFY_OCTAL_PERMISSIONS(perms) (perms)
static struct kobj_attribute g40_attr = __ATTR(mode40, 0777, mode_show40, mode_store40);


/** The ebb_attrs[] is an array of attributes that is used to create the attribute group below.
 *  The attr property of the kobj_attribute is used to extract the attribute struct
 */

static struct attribute *g7_attrs[] = {
   &g7_attr.attr,                  
   NULL,
};

static struct attribute *g11_attrs[] = {
   &g11_attr.attr,                  
   NULL,
};

static struct attribute *g13_attrs[] = {
   &g13_attr.attr,                  
   NULL,
};

static struct attribute *g15_attrs[] = {
   &g15_attr.attr,                  
   NULL,
};

static struct attribute *g16_attrs[] = {
   &g16_attr.attr,                  
   NULL,
};

static struct attribute *g18_attrs[] = {
   &g18_attr.attr,                  
   NULL,
};

static struct attribute *g22_attrs[] = {
   &g22_attr.attr,                  
   NULL,
};

static struct attribute *g26_attrs[] = {
   &g26_attr.attr,                  
   NULL,
};

static struct attribute *g37_attrs[] = {
   &g37_attr.attr,                  
   NULL,
};

static struct attribute *g38_attrs[] = {
   &g38_attr.attr,                  
   NULL,
};

static struct attribute *g40_attrs[] = {
   &g40_attr.attr,                  
   NULL,
};



/** The attribute group uses the attribute array and a name, which is exposed on sysfs -- in this
 *  case it is gpio, which is automatically defined in the _init() function below
 *  using the custom kernel parameter that can be passed when the module is loaded.
 */
static struct attribute_group attr_group_7 = {
   .name  = gpioName7,                        // The name is generated in _init()
   .attrs = g7_attrs,                      // The attributes array defined just above
};

static struct attribute_group attr_group_11 = {
   .name  = gpioName11,                        // The name is generated in _init()
   .attrs = g11_attrs,                      // The attributes array defined just above
};

static struct attribute_group attr_group_13 = {
   .name  = gpioName13,                        // The name is generated in _init()
   .attrs = g13_attrs,                      // The attributes array defined just above
};

static struct attribute_group attr_group_15 = {
   .name  = gpioName15,                        // The name is generated in _init()
   .attrs = g15_attrs,                      // The attributes array defined just above
};

static struct attribute_group attr_group_16 = {
   .name  = gpioName16,                        // The name is generated in _init()
   .attrs = g16_attrs,                      // The attributes array defined just above
};

static struct attribute_group attr_group_18 = {
   .name  = gpioName18,                        // The name is generated in _init()
   .attrs = g18_attrs,                      // The attributes array defined just above
};

static struct attribute_group attr_group_22 = {
   .name  = gpioName22,                        // The name is generated in _init()
   .attrs = g22_attrs,                      // The attributes array defined just above
};

static struct attribute_group attr_group_26 = {
   .name  = gpioName26,                        // The name is generated in _init()
   .attrs = g26_attrs,                      // The attributes array defined just above
};

static struct attribute_group attr_group_37 = {
   .name  = gpioName37,                        // The name is generated in _init()
   .attrs = g37_attrs,                      // The attributes array defined just above
};

static struct attribute_group attr_group_38 = {
   .name  = gpioName38,                        // The name is generated in _init()
   .attrs = g38_attrs,                      // The attributes array defined just above
};

static struct attribute_group attr_group_40 = {
   .name  = gpioName40,                        // The name is generated in _init()
   .attrs = g40_attrs,                      // The attributes array defined just above
};

static struct kobject *nym_kobj;            /// The pointer to the kobject
static struct task_struct *task;            /// The pointer to the thread task

/** @brief The  main kthread loop
 *
 *  @param arg A void pointer used in order to pass data to the thread
 *  @return returns 0 if successful
 */
static int flash(void *arg){
   printk(KERN_INFO "Nymble GPIO: Thread has started running \n");
   while(!kthread_should_stop()){           // Returns true when kthread_stop() is called
      set_current_state(TASK_RUNNING);
      if (mode7==RESET){ gpio_set_value(gpio7,false);msleep(100);gpio_set_value(gpio7,true);mode7 =NSTATE;}      // Invert the  state
      else if (mode7==ON){gpio_set_value(gpio7,true);mode7 =NSTATE;}
      else if(mode7==OFF){gpio_set_value(gpio7,false);mode7 =NSTATE;}
      else if (mode11==RESET){ gpio_set_value(gpio11,false);msleep(100);gpio_set_value(gpio11,true);mode11 =NSTATE;}      // Invert the  state
      else if (mode11==ON){gpio_set_value(gpio11,true);mode11 =NSTATE;}
      else if(mode11==OFF){gpio_set_value(gpio11,false);mode11 =NSTATE;}
      else if (mode13==RESET){ gpio_set_value(gpio13,false);msleep(100);gpio_set_value(gpio13,true);mode13 =NSTATE;}      // Invert the  state
      else if (mode13==ON){gpio_set_value(gpio13,true);mode13 =NSTATE;}
      else if(mode13==OFF){gpio_set_value(gpio13,false);mode13 =NSTATE;}
      else if (mode15==RESET){ gpio_set_value(gpio15,false);msleep(100);gpio_set_value(gpio15,true);mode15 =NSTATE;}      // Invert the  state
      else if (mode15==ON){gpio_set_value(gpio15,true);mode15 =NSTATE;}
      else if(mode15==OFF){gpio_set_value(gpio15,false);mode15 =NSTATE;}
      else if (mode16==RESET){ gpio_set_value(gpio16,false);msleep(100);gpio_set_value(gpio16,true);mode16 =NSTATE;}      // Invert the  state
      else if (mode16==ON){gpio_set_value(gpio16,true);mode16 =NSTATE;}
      else if(mode16==OFF){gpio_set_value(gpio16,false);mode16 =NSTATE;}
      else if (mode18==RESET){ gpio_set_value(gpio18,false);msleep(100);gpio_set_value(gpio18,true);mode18 =NSTATE;}      // Invert the  state
      else if (mode18==ON){gpio_set_value(gpio18,true);mode18 =NSTATE;}
      else if(mode18==OFF){gpio_set_value(gpio18,false);mode18 =NSTATE;}
      else if (mode22==RESET){ gpio_set_value(gpio22,false);msleep(100);gpio_set_value(gpio22,true);mode22 =NSTATE;}      // Invert the  state
      else if (mode22==ON){gpio_set_value(gpio22,true);mode22 =NSTATE;}
      else if(mode22==OFF){gpio_set_value(gpio22,false);mode22 =NSTATE;}
      else if (mode26==RESET){ gpio_set_value(gpio26,false);msleep(100);gpio_set_value(gpio26,true);mode26 =NSTATE;}      // Invert the  state
      else if (mode26==ON){gpio_set_value(gpio26,true);mode26 =NSTATE;}
      else if(mode26==OFF){gpio_set_value(gpio26,false);mode26 =NSTATE;}
      else if (mode37==RESET){ gpio_set_value(gpio37,false);msleep(100);gpio_set_value(gpio37,true);mode37 =NSTATE;}      // Invert the  state
      else if (mode37==ON){gpio_set_value(gpio37,true);mode37 =NSTATE;}
      else if(mode37==OFF){gpio_set_value(gpio37,false);mode37 =NSTATE;}
      else if (mode38==RESET){ gpio_set_value(gpio38,false);msleep(100);gpio_set_value(gpio38,true);mode38 =NSTATE;}      // Invert the  state
      else if (mode38==ON){gpio_set_value(gpio38,true);mode38 =NSTATE;}
      else if(mode38==OFF){gpio_set_value(gpio38,false);mode38 =NSTATE;}
      else if (mode40==RESET){ gpio_set_value(gpio40,false);msleep(100);gpio_set_value(gpio40,true);mode40 =NSTATE;}      // Invert the  state
      else if (mode40==ON){gpio_set_value(gpio40,true);mode40 =NSTATE;}
      else if(mode40==OFF){gpio_set_value(gpio40,false);mode40 =NSTATE;}

             // Use the  state to light/turn off the 
      set_current_state(TASK_INTERRUPTIBLE);
      msleep(100);                // millisecond sleep for half of the period
   }
   printk(KERN_INFO "Nymble GPIO: Thread has run to completion \n");
   return 0;
}

/** @brief The LKM initialization function
 *  The static keyword restricts the visibility of the function to within this C file. The __init
 *  macro means that for a built-in driver (not a LKM) the function is only used at initialization
 *  time and that it can be discarded and its memory freed up after that point. In this example this
 *  function sets up the GPIOs and the IRQ
 *  @return returns 0 if successful
 */
static int __init nymresetdriver_init(void){
   int result = 0;

   printk(KERN_INFO "Nymble GPIO: Initializing the Nymble GPIO LKM\n");
   sprintf(gpioName7, "gpio%d", gpio7);      // Create the gpio  name for /sys/ 
   sprintf(gpioName11, "gpio%d", gpio11);
   sprintf(gpioName13, "gpio%d", gpio13);      // Create the gpio  name for /sys/ 
   sprintf(gpioName15, "gpio%d", gpio15);
   sprintf(gpioName16, "gpio%d", gpio16);      // Create the gpio  name for /sys/ 
   sprintf(gpioName18, "gpio%d", gpio18);
   sprintf(gpioName22, "gpio%d", gpio22);      // Create the gpio  name for /sys/ 
   sprintf(gpioName26, "gpio%d", gpio26);
   sprintf(gpioName37, "gpio%d", gpio37);
   sprintf(gpioName38, "gpio%d", gpio38);
   sprintf(gpioName40, "gpio%d", gpio40);


   nym_kobj = kobject_create_and_add("ngpio", kernel_kobj->parent); // kernel_kobj points to /sys/kernel
   if(!nym_kobj){
      printk(KERN_ALERT "Nymble GPIO: failed to create kobject\n");
      return -ENOMEM;
   }

   result = sysfs_create_group(nym_kobj, &attr_group_7);
   if(result) {
      printk(KERN_ALERT "Nymble GPIO: failed to create sysfs group\n");
      kobject_put(nym_kobj);                // clean up -- remove the kobject sysfs entry
      return result;
   }

   result = sysfs_create_group(nym_kobj, &attr_group_11);
   if(result) {
      printk(KERN_ALERT "Nymble GPIO: failed to create sysfs group\n");
      kobject_put(nym_kobj);                // clean up -- remove the kobject sysfs entry
      return result;
   }

   result = sysfs_create_group(nym_kobj, &attr_group_13);
   if(result) {
      printk(KERN_ALERT "Nymble GPIO: failed to create sysfs group\n");
      kobject_put(nym_kobj);                // clean up -- remove the kobject sysfs entry
      return result;
   }

   result = sysfs_create_group(nym_kobj, &attr_group_15);
   if(result) {
      printk(KERN_ALERT "Nymble GPIO: failed to create sysfs group\n");
      kobject_put(nym_kobj);                // clean up -- remove the kobject sysfs entry
      return result;
   }

   result = sysfs_create_group(nym_kobj, &attr_group_16);
   if(result) {
      printk(KERN_ALERT "Nymble GPIO: failed to create sysfs group\n");
      kobject_put(nym_kobj);                // clean up -- remove the kobject sysfs entry
      return result;
   }

   result = sysfs_create_group(nym_kobj, &attr_group_18);
   if(result) {
      printk(KERN_ALERT "Nymble GPIO: failed to create sysfs group\n");
      kobject_put(nym_kobj);                // clean up -- remove the kobject sysfs entry
      return result;
   }

   result = sysfs_create_group(nym_kobj, &attr_group_22);
   if(result) {
      printk(KERN_ALERT "Nymble GPIO: failed to create sysfs group\n");
      kobject_put(nym_kobj);                // clean up -- remove the kobject sysfs entry
      return result;
   }

   result = sysfs_create_group(nym_kobj, &attr_group_26);
   if(result) {
      printk(KERN_ALERT "Nymble GPIO: failed to create sysfs group\n");
      kobject_put(nym_kobj);                // clean up -- remove the kobject sysfs entry
      return result;
   }

   result = sysfs_create_group(nym_kobj, &attr_group_37);
   if(result) {
      printk(KERN_ALERT "Nymble GPIO: failed to create sysfs group\n");
      kobject_put(nym_kobj);                // clean up -- remove the kobject sysfs entry
      return result;
   }

   result = sysfs_create_group(nym_kobj, &attr_group_38);
   if(result) {
      printk(KERN_ALERT "Nymble GPIO: failed to create sysfs group\n");
      kobject_put(nym_kobj);                // clean up -- remove the kobject sysfs entry
      return result;
   }

   result = sysfs_create_group(nym_kobj, &attr_group_40);
   if(result) {
      printk(KERN_ALERT "Nymble GPIO: failed to create sysfs group\n");
      kobject_put(nym_kobj);                // clean up -- remove the kobject sysfs entry
      return result;
   }

   gpioOn = false;
   gpio_request(gpio7, "sysfs");          // gpio , request it
   gpio_direction_output(gpio7,gpioOn);   // Set the gpio to be in output mode and turn off
   gpio_export(gpio7, false);  // causes gpio  to appear in /sys/class/gpio

   gpioOn = false;
   gpio_request(gpio11, "sysfs");          // gpio , request it
   gpio_direction_output(gpio11,gpioOn);   // Set the gpio to be in output mode and turn off
   gpio_export(gpio11, false);  // causes gpio  to appear in /sys/class/gpio

   gpioOn = false;
   gpio_request(gpio13, "sysfs");          // gpio , request it
   gpio_direction_output(gpio13,gpioOn);   // Set the gpio to be in output mode and turn off
   gpio_export(gpio13, false);  // causes gpio  to appear in /sys/class/gpio

   gpioOn = false;
   gpio_request(gpio15, "sysfs");          // gpio , request it
   gpio_direction_output(gpio15,gpioOn);   // Set the gpio to be in output mode and turn off
   gpio_export(gpio15, false);  // causes gpio  to appear in /sys/class/gpio

   gpioOn = false;
   gpio_request(gpio16, "sysfs");          // gpio , request it
   gpio_direction_output(gpio16,gpioOn);   // Set the gpio to be in output mode and turn off
   gpio_export(gpio16, false);  // causes gpio  to appear in /sys/class/gpio

   gpioOn = false;
   gpio_request(gpio18, "sysfs");          // gpio , request it
   gpio_direction_output(gpio18,gpioOn);   // Set the gpio to be in output mode and turn off
   gpio_export(gpio18, false);  // causes gpio  to appear in /sys/class/gpio

   gpioOn = false;
   gpio_request(gpio22, "sysfs");          // gpio , request it
   gpio_direction_output(gpio22,gpioOn);   // Set the gpio to be in output mode and turn off
   gpio_export(gpio22, false);  // causes gpio  to appear in /sys/class/gpio

   gpioOn = false;
   gpio_request(gpio26, "sysfs");          // gpio , request it
   gpio_direction_output(gpio26,gpioOn);   // Set the gpio to be in output mode and turn off
   gpio_export(gpio26, false);  // causes gpio  to appear in /sys/class/gpio

   gpioOn = false;
   gpio_request(gpio37, "sysfs");          // gpio , request it
   gpio_direction_output(gpio37,gpioOn);   // Set the gpio to be in output mode and turn off
   gpio_export(gpio37, false);  // causes gpio  to appear in /sys/class/gpio

   gpioOn = false;
   gpio_request(gpio38, "sysfs");          // gpio , request it
   gpio_direction_output(gpio38,gpioOn);   // Set the gpio to be in output mode and turn off
   gpio_export(gpio38, false);  // causes gpio  to appear in /sys/class/gpio

   gpioOn = false;
   gpio_request(gpio40, "sysfs");          // gpio , request it
   gpio_direction_output(gpio40,gpioOn);   // Set the gpio to be in output mode and turn off
   gpio_export(gpio40, false);  // causes gpio  to appear in /sys/class/gpio

   task = kthread_run(flash, NULL, "gpIO_thread");  // Start the     thread
   if(IS_ERR(task)){                                     // Kthread name    
      printk(KERN_ALERT "Nymble GPIO: failed to create the task\n");
      return PTR_ERR(task);
   }
   return result;
}

/** @brief The LKM cleanup function
 *  Similar to the initialization function, it is static. The __exit macro notifies that if this
 *  code is used for a built-in driver (not a LKM) that this function is not required.
 */
static void __exit nymresetdriver_exit(void){
   kthread_stop(task);                      // Stop the LED flashing thread
   kobject_put(nym_kobj);                   // clean up -- remove the kobject sysfs entry
   gpio_set_value(gpio7, 0);
   gpio_set_value(gpio11, 0);
   gpio_set_value(gpio13, 0);
   gpio_set_value(gpio15, 0);
   gpio_set_value(gpio16, 0);
   gpio_set_value(gpio18, 0);
   gpio_set_value(gpio22, 0);
   gpio_set_value(gpio26, 0);
   gpio_set_value(gpio37, 0);
   gpio_set_value(gpio38, 0);
   gpio_set_value(gpio40, 0);
   gpio_unexport(gpio7);
   gpio_unexport(gpio11);
   gpio_unexport(gpio13);
   gpio_unexport(gpio15);
   gpio_unexport(gpio16);
   gpio_unexport(gpio18);
   gpio_unexport(gpio22);
   gpio_unexport(gpio26);
   gpio_unexport(gpio37);
   gpio_unexport(gpio38);
   gpio_unexport(gpio40);
   gpio_free(gpio7);
   gpio_free(gpio11);
   gpio_free(gpio13);
   gpio_free(gpio15);
   gpio_free(gpio16);
   gpio_free(gpio18);
   gpio_free(gpio22);
   gpio_free(gpio26);
   gpio_free(gpio37);
   gpio_free(gpio38);
   gpio_free(gpio40);
   printk(KERN_INFO "Nymble GPIO: Goodbye from the Nymble GPIO LKM!\n");
}

/// This next calls are  mandatory -- they identify the initialization function
/// and the cleanup function (as above).
module_init(nymresetdriver_init);
module_exit(nymresetdriver_exit);
