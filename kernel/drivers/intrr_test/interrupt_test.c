/**
 * @file   
 * @author 
 * @date   
 * @brief  A kernel module for controlling a gpio (or any signal) that is connected to
 * a external mcu reset pin. It is threaded in order that it can three modes reset ,off and on.
 * The sysfs entry appears at /sys/ngpio/mod*
 * @see 
*/

#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/gpio.h>       // Required for the GPIO functions
#include <linux/kobject.h>    // Using kobjects for the sysfs bindings
#include <linux/kthread.h>    // Using kthreads for the flashing functionality
#include <linux/delay.h>      // Using this header for the msleep() function
#include <linux/interrupt.h>  // Required for the IRQ code
#include <linux/time.h>       // Using the clock to measure time between button presses
#define  DEBOUNCE_TIME 200    ///< The default bounce time -- 200ms

MODULE_LICENSE("GPL");
MODULE_AUTHOR("sijok");
MODULE_DESCRIPTION("GPIO interrupt Test ");
MODULE_VERSION("0.1");

 
static bool isRising = 1;                   ///< Rising edge is the default IRQ property
module_param(isRising, bool, S_IRUGO);      ///< Param desc. S_IRUGO can be read/not changed
MODULE_PARM_DESC(isRising, " Rising edge = 1 (default), Falling edge = 0");  ///< parameter description
 
static unsigned int interruptPin = 36;       
module_param(interruptPin, uint, S_IRUGO);    ///< Param desc. S_IRUGO can be read/not changed
MODULE_PARM_DESC(interruptPin, " Interrupt number )");  ///< parameter description
 
static unsigned int actGpio = 32;           
module_param(actGpio, uint, S_IRUGO);       ///< Param desc. S_IRUGO can be read/not changed
MODULE_PARM_DESC(actGpio, " ");         ///< parameter description
 
static int    irqNumber;                    ///< Used to share the IRQ number within this file
static int    numberPresses = 0;            ///< For information, store the number of button presses
static bool   isDebounce = 1;               ///< Use to store the debounce state (on by default)
static struct timespec ts_last, ts_current, ts_diff;  ///< timespecs from linux/time.h (has nano precision)
static bool   ledOn = 0;                    ///< Is the LED on or off? Used to invert its state (off by default)
/// Function prototype for the custom IRQ handler function -- see below for the implementation
static irq_handler_t  test_irq_handler(unsigned int irq, void *dev_id, struct pt_regs *regs);
 
 
/** @brief The LKM initialization function
 *  The static keyword restricts the visibility of the function to within this C file. The __init
 *  macro means that for a built-in driver (not a LKM) the function is only used at initialization
 *  time and that it can be discarded and its memory freed up after that point. In this example this
 *  function sets up the GPIOs and the IRQ
 *  @return returns 0 if successful
 */
static int __init testmodule_init(void){
   int result = 0;
   unsigned long IRQflags = IRQF_TRIGGER_RISING;      // The default is a rising-edge interrupt
 
   printk(KERN_INFO "TEST MOdule: Initializing \n");
 
   getnstimeofday(&ts_last);                          // set the last time to be the current time
   ts_diff = timespec_sub(ts_last, ts_last);          // set the initial time difference to be 0
 

   gpio_request(actGpio, "sysfs");          
   gpio_direction_output(actGpio, true);   // Set the gpio to be in output mode and on
// gpio_set_value(gpioLED, ledOn);          // Not required as set by line above (here for reference)
   gpio_export(actGpio, true);             // Causes gpio49 to appear in /sys/class/gpio
                     // the bool argument prevents the direction from being changed
   gpio_request(interruptPin, "sysfs");       // Set up the gpioButton
   gpio_direction_input(interruptPin);        // Set the button GPIO to be an input
   gpio_set_debounce(interruptPin, DEBOUNCE_TIME); // Debounce the button with a delay of 200ms
   gpio_export(interruptPin, false);          // Causes  to appear in /sys/class/gpio
                     // the bool argument prevents the direction from being changed
 
   // Perform a quick test to see that the button is working as expected on LKM load
   printk(KERN_INFO "TEST MOdule: The button state is currently: %d\n", gpio_get_value(interruptPin));
 
   /// GPIO numbers and IRQ numbers are not the same! This function performs the mapping for us
   irqNumber = gpio_to_irq(interruptPin);
   printk(KERN_INFO "TEST MOdule: The button is mapped to IRQ: %d\n", irqNumber);
 
   if(!isRising){                           // If the kernel parameter isRising=0 is supplied
      IRQflags = IRQF_TRIGGER_FALLING;      // Set the interrupt to be on the falling edge
   }
   // This next call requests an interrupt line
   result = request_irq(irqNumber,             // The interrupt number requested
                        (irq_handler_t) test_irq_handler, // The pointer to the handler function below
                        IRQflags,              // Use the custom kernel param to set interrupt type
                        "test_interrupt_handler",  // Used in /proc/interrupts to identify the owner
                        NULL);                 // The *dev_id for shared interrupt lines, NULL is okay
   return result;
}
 
/** @brief The LKM cleanup function
 *  Similar to the initialization function, it is static. The __exit macro notifies that if this
 *  code is used for a built-in driver (not a LKM) that this function is not required.
 */
static void __exit testmodule_exit(void){
   printk(KERN_INFO "TEST MOdule: The button was pressed %d times\n", numberPresses);

   gpio_set_value(interruptPin, 0);              // Turn the LED off, makes it clear the device was unloaded
   gpio_unexport(interruptPin);                  // Unexport the LED GPIO
   free_irq(irqNumber, NULL); 

   gpio_set_value(actGpio, 0);             // Free the IRQ number, no *dev_id required in this case
   gpio_unexport(actGpio);               // Unexport the Button GPIO
   gpio_free(actGpio);                      // Free the LED GPIO
   gpio_free(interruptPin);                   // Free the Button GPIO
   printk(KERN_INFO "TEST MOdule: un init TEST MOdule LKM!\n");
}
 
/** @brief The GPIO IRQ Handler function
 *  This function is a custom interrupt handler that is attached to the GPIO above. The same interrupt
 *  handler cannot be invoked concurrently as the interrupt line is masked out until the function is complete.
 *  This function is static as it should not be invoked directly from outside of this file.
 *  @param irq    the IRQ number that is associated with the GPIO -- useful for logging.
 *  @param dev_id the *dev_id that is provided -- can be used to identify which device caused the interrupt
 *  Not used in this example as NULL is passed.
 *  @param regs   h/w specific register values -- only really ever used for debugging.
 *  return returns IRQ_HANDLED if successful -- should return IRQ_NONE otherwise.
 */
static irq_handler_t test_irq_handler(unsigned int irq, void *dev_id, struct pt_regs *regs){
   ledOn = !ledOn;
   //ledOn= false;
   gpio_set_value(actGpio,ledOn);      // Set the physical LED accordingly
   getnstimeofday(&ts_current);         // Get the current time as ts_current
 //  ts_diff = timespec_sub(ts_current, ts_last);   // Determine the time difference between last 2 presses
 //  ts_last = ts_current;                // Store the current time as the last time ts_last
   printk(KERN_INFO "TEST MOdule..: The state is currently: %d\n", gpio_get_value(interruptPin));
 //  numberPresses++;                     // Global counter, will be outputted when the module is unloaded
   return (irq_handler_t) IRQ_HANDLED;  // Announce that the IRQ has been handled correctly
}
 
// This next calls are  mandatory -- they identify the initialization function
// and the cleanup function (as above).
module_init(testmodule_init);
module_exit(testmodule_exit);