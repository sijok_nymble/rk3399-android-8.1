// SPDX-License-Identifier: GPL-2.0
/* Copyright (c) 2018, The Linux Foundation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 and
 * only version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 */

#include <linux/gpio/consumer.h>
#include <linux/of_graph.h>
#include <linux/regulator/consumer.h>
#include <linux/pinctrl/consumer.h>

#include <video/mipi_display.h>
#include <video/of_videomode.h>
#include <video/videomode.h>

#include <drm/drmP.h>
#include <drm/drm_panel.h>
#include <drm/drm_mipi_dsi.h>

static const char * const regulator_names[] = {
	"vdda",
	"vdispp",
	"vdispn"
};

static unsigned long regulator_enable_loads[] = {
	62000,
	100000,
	100000
};

static unsigned long regulator_disable_loads[] = {
	80,
	100,
	100
};

struct truly_wqxga {
	struct device *dev;
	struct drm_panel panel;

	struct regulator_bulk_data supplies[ARRAY_SIZE(regulator_names)];

	struct gpio_desc *reset_gpio;
	struct gpio_desc *mode_gpio;

	struct backlight_device *backlight;
	struct videomode vm;

	struct mipi_dsi_device *dsi[2];

	bool prepared;
	bool enabled;
};

static inline struct truly_wqxga *panel_to_truly_wqxga(struct drm_panel *panel)
{
	return container_of(panel, struct truly_wqxga, panel);
}

static int truly_wqxga_power_on(struct truly_wqxga *ctx)
{
	int ret, i;

	for (i = 0; i < ARRAY_SIZE(ctx->supplies); i++) {
		ret = regulator_set_load(ctx->supplies[i].consumer,
					regulator_enable_loads[i]);
		if (ret)
			return ret;
	}

	ret = regulator_bulk_enable(ARRAY_SIZE(ctx->supplies), ctx->supplies);
	if (ret < 0)
		return ret;

	msleep(20);
	gpiod_set_value(ctx->reset_gpio, 1);
	msleep(20);
	gpiod_set_value(ctx->reset_gpio, 0);
	msleep(20);
	gpiod_set_value(ctx->reset_gpio, 1);
	msleep(50);

	return 0;
}

static int truly_wqxga_power_off(struct truly_wqxga *ctx)
{
	int ret, i;

	gpiod_set_value(ctx->reset_gpio, 0);

	for (i = 0; i < ARRAY_SIZE(ctx->supplies); i++) {
		ret = regulator_set_load(ctx->supplies[i].consumer,
				regulator_disable_loads[i]);
		if (ret)
			return ret;
	}

	return regulator_bulk_disable(ARRAY_SIZE(ctx->supplies), ctx->supplies);
}

static int truly_wqxga_disable(struct drm_panel *panel)
{
	struct truly_wqxga *ctx = panel_to_truly_wqxga(panel);

	if (!ctx->enabled)
		return 0;

	if (ctx->backlight) {
		ctx->backlight->props.power = FB_BLANK_POWERDOWN;
		backlight_update_status(ctx->backlight);
	}

	ctx->enabled = false;
	return 0;
}

static int truly_wqxga_unprepare(struct drm_panel *panel)
{
	struct truly_wqxga *ctx = panel_to_truly_wqxga(panel);
	struct mipi_dsi_device **dsis = ctx->dsi;
	int ret = 0, i;

	if (!ctx->prepared)
		return 0;

	dsis[0]->mode_flags = 0;
	dsis[1]->mode_flags = 0;

	for (i = 0; i < 2; i++)
		if (mipi_dsi_dcs_set_display_off(dsis[i]) < 0)
			ret = -ECOMM;

	msleep(120);

	for (i = 0; i < 2; i++)
		if (mipi_dsi_dcs_enter_sleep_mode(dsis[i]) < 0)
			ret = -ECOMM;

	truly_wqxga_power_off(ctx);

	ctx->prepared = false;
	return ret;
}

#define MAX_LEN	6
struct {
	u8 commands[MAX_LEN];
	int size;
	} panel_cmds[] = { /* CMD2_P0 */
						{{ 0xFF,0xAA,0x55,0xA5,0x80},5},
						{{ 0x6F,0x11,0x00},3},
						{{ 0xF7,0x20,0x00},3},
						{{ 0x6F,0x06},2},
						{{ 0xF7,0xA0},2},
						{{ 0x6F,0x19},2},
						{{ 0xF7,0x12},2},
						{{ 0xF4,0x03},2},
						{{ 0x6F,0x08},2},
						{{ 0xFA,0x40},2},
						{{ 0x6F,0x11},2},
						{{ 0xF3,0x01},2},
						{{ 0xF0,0x55,0xAA,0x52,0x08,0x00},6},
						{{ 0xC8,0x80},2},
						{{ 0xB1,0x6C,0x01},3},
						{{ 0xB6,0x08},2},
						{{ 0x6F,0x02},2},
						{{ 0xB8,0x08},2},
						{{ 0xBB,0x54,0x54},3},
						{{ 0xBC,0x05,0x05},3},
						{{ 0xC7,0x01},2},
						{{ 0xBD,0x02,0xB0,0x0C,0x0A,0x00},6},
						{{ 0xF0,0x55,0xAA,0x52,0x08,0x01},6},
						{{ 0xB0,0x05,0x05},3},
						{{ 0xB1,0x05,0x05},3},
						{{ 0xBC,0x88,0x01},3},
						{{ 0xBD,0x88,0x01},3},
						{{ 0xCA,0x00},2},
						{{ 0xC0,0x04},2},
						{{ 0xB2,0x00,0x00},3},
						{{ 0xBE,0x80},2},
						{{ 0xB3,0x19,0x19},3},
						{{ 0xB4,0x12,0x12},3},
						{{ 0xB9,0x24,0x24},3},
						{{ 0xBA,0x14,0x14},3},
						{{ 0xF0,0x55,0xAA,0x52,0x08,0x02},6},
						{{ 0xEE,0x01},2},
						{{ 0xEF,0x09,0x06,0x15,0x18},5},
						{{ 0xB0,0x00,0x00,0x00,0x0D,0x00,0x22},7},
						{{ 0x6F,0x06},2},
						{{ 0xB0,0x00,0x33,0x00,0x41,0x00,0x58},7},
						{{ 0x6F,0x0C},2},
						{{ 0xB0,0x00,0x6B,0x00,0x9D},5},
						{{ 0xB1,0x00,0xC6,0x01,0x08,0x01,0x3D},7},
						{{ 0x6F,0x06},2},
						{{ 0xB1,0x01,0x92,0x01,0xD7,0x01,0xD9},7},
						{{ 0x6F,0x0C},2},
						{{ 0xB1,0x02,0x1A,0x02,0x65},5},
						{{ 0xB2,0x02,0x96,0x02,0xD7,0x03,0x04},7},
						{{ 0x6F,0x06},2},
						{{ 0xB2,0x03,0x40,0x03,0x67,0x03,0x90},7},
						{{ 0x6F,0x0C},2},
						{{ 0xB2,0x03,0xA6,0x03,0xC1},5},
						{{ 0xB3,0x03,0xEA,0x03,0xFF},5},
						{{ 0xF0,0x55,0xAA,0x52,0x08,0x06},6},
						{{ 0xB0,0x00,0x17},3},
						{{ 0xB1,0x16,0x15},3},
						{{ 0xB2,0x14,0x13},3},
						{{ 0xB3,0x12,0x11},3},
						{{ 0xB4,0x10,0x2D},3},
						{{ 0xB5,0x01,0x08},3},
						{{ 0xB6,0x09,0x31},3},
						{{ 0xB7,0x31,0x31},3},
						{{ 0xB8,0x31,0x31},3},
						{{ 0xB9,0x31,0x31},3},
						{{ 0xBA,0x31,0x31},3},
						{{ 0xBB,0x31,0x31},3},
						{{ 0xBC,0x31,0x31},3},
						{{ 0xBD,0x31,0x09},3},
						{{ 0xBE,0x08,0x01},3},
						{{ 0xBF,0x2D,0x10},3},
						{{ 0xC0,0x11,0x12},3},
						{{ 0xC1,0x13,0x14},3},
						{{ 0xC2,0x15,0x16},3},
						{{ 0xC3,0x17,0x00},3},
						{{ 0xE5,0x31,0x31},3},
						{{ 0xC4,0x00,0x17},3},
						{{ 0xC5,0x16,0x15},3},
						{{ 0xC6,0x14,0x13},3},
						{{ 0xC7,0x12,0x11},3},
						{{ 0xC8,0x10,0x2D},3},
						{{ 0xC9,0x01,0x08},3},
						{{ 0xCA,0x09,0x31},3},
						{{ 0xCB,0x31,0x31},3},
						{{ 0xCC,0x31,0x31},3},
						{{ 0xCD,0x31,0x31},3},
						{{ 0xCE,0x31,0x31},3},
						{{ 0xCF,0x31,0x31},3},
						{{ 0xD0,0x31,0x31},3},
						{{ 0xD1,0x31,0x09},3},
						{{ 0xD2,0x08,0x01},3},
						{{ 0xD3,0x2D,0x10},3},
						{{ 0xD4,0x11,0x12},3},
						{{ 0xD5,0x13,0x14},3},
						{{ 0xD6,0x15,0x16},3},
						{{ 0xD7,0x17,0x00},3},
						{{ 0xE6,0x31,0x31},3},
						{{ 0xD8,0x00,0x00,0x00,0x00,0x00},6},
						{{ 0xD9,0x00,0x00,0x00,0x00,0x00},6},
						{{ 0xE7,0x00},2},
						{{ 0xF0,0x55,0xAA,0x52,0x08,0x03},6},
						{{ 0xB0,0x20,0x00},3},
						{{ 0xB1,0x20,0x00},3},
						{{ 0xB2,0x05,0x00,0x42,0x00,0x00},6},
						{{ 0xB6,0x05,0x00,0x42,0x00,0x00},6},
						{{ 0xBA,0x53,0x00,0x42,0x00,0x00},6},
						{{ 0xBB,0x53,0x00,0x42,0x00,0x00},6},
						{{ 0xC4,0x40},2},
						{{ 0xF0,0x55,0xAA,0x52,0x08,0x05},6},
						{{ 0xB0,0x17,0x06},3},
						{{ 0xB8,0x00},2},
						{{ 0xBD,0x03,0x01,0x01,0x00,0x01},6},
						{{ 0xB1,0x17,0x06},3},
						{{ 0xB9,0x00,0x01},3},
						{{ 0xB2,0x17,0x06},3},
						{{ 0xBA,0x00,0x01},3},
						{{ 0xB3,0x17,0x06},3},
						{{ 0xBB,0x0A,0x00},3},
						{{ 0xB4,0x17,0x06},3},
						{{ 0xB5,0x17,0x06},3},
						{{ 0xB6,0x14,0x03},3},
						{{ 0xB7,0x00,0x00},3},
						{{ 0xBC,0x02,0x01},3},
						{{ 0xC0,0x05},2},
						{{ 0xC4,0xA5},2},
						{{ 0xC8,0x03,0x30},3},
						{{ 0xC9,0x03,0x51},3},
						{{ 0xD1,0x00,0x05,0x03,0x00,0x00},6},
						{{ 0xD2,0x00,0x05,0x09,0x00,0x00},6},
						{{ 0xE5,0x02},2},
						{{ 0xE6,0x02},2},
						{{ 0xE7,0x02},2},
						{{ 0xE9,0x02},2},
						{{ 0xED,0x33},2},
						{{  0x11},1},
						{{  0x29},1},

};

static int truly_wqxga_prepare(struct drm_panel *panel)
{
	struct truly_wqxga *ctx = panel_to_truly_wqxga(panel);
	struct mipi_dsi_device **dsis = ctx->dsi;
	struct mipi_dsi_device *d;
	int ret, i, j;

	if (ctx->prepared)
		return 0;

	ret = truly_wqxga_power_on(ctx);
	if (ret < 0)
		return ret;

	dsis[0]->mode_flags |= MIPI_DSI_MODE_LPM;
	dsis[1]->mode_flags |= MIPI_DSI_MODE_LPM;

	for (j = 0; j < ARRAY_SIZE(panel_cmds); j++) {
		for (i = 0; i < 2; i++) {
			d = dsis[i];
			ret = mipi_dsi_dcs_write_buffer(dsis[i],panel_cmds[j].commands,panel_cmds[j].size);
			if (ret < 0) {
				dev_err(ctx->dev, "failed to cmd no %d, err: %d\n",
						j, ret);
				return ret;
			}
		}
	}

	for (i = 0; i < 2; i++)
		if (mipi_dsi_dcs_exit_sleep_mode(dsis[i]) < 0) {
			dev_err(ctx->dev, "failed to exit sleep mode\n");
			return -ECOMM;
		}
	msleep(78);

	for (i = 0; i < 2; i++)
		if (mipi_dsi_dcs_set_display_on(dsis[i]) < 0) {
			dev_err(ctx->dev, "failed to send display on\n");
			return -ECOMM;
		}
	msleep(78);

	ctx->prepared = true;

	return 0;
}

static int truly_wqxga_enable(struct drm_panel *panel)
{
	struct truly_wqxga *ctx = panel_to_truly_wqxga(panel);

	if (ctx->enabled)
		return 0;

	if (ctx->backlight) {
		ctx->backlight->props.power = FB_BLANK_UNBLANK;
		backlight_update_status(ctx->backlight);
	}
	ctx->enabled = true;

	return 0;
}

static int truly_wqxga_get_modes(struct drm_panel *panel)
{
	struct drm_connector *connector = panel->connector;
	struct truly_wqxga *ctx = panel_to_truly_wqxga(panel);
	struct drm_display_mode *mode;

	mode = drm_mode_create(connector->dev);
	if (!mode) {
		dev_err(ctx->dev, "failed to create a new display mode\n");
		return 0;
	}

	drm_display_mode_from_videomode(&ctx->vm, mode);
	connector->display_info.width_mm = 74;
	connector->display_info.height_mm = 131;
	mode->type = DRM_MODE_TYPE_DRIVER | DRM_MODE_TYPE_PREFERRED;
	drm_mode_probed_add(connector, mode);

	return 1;
}

static const struct drm_panel_funcs truly_wqxga_drm_funcs = {
	.disable = truly_wqxga_disable,
	.unprepare = truly_wqxga_unprepare,
	.prepare = truly_wqxga_prepare,
	.enable = truly_wqxga_enable,
	.get_modes = truly_wqxga_get_modes,
};

static int truly_wqxga_panel_add(struct truly_wqxga *ctx)
{
	struct device *dev = ctx->dev;
	int ret, i;

	for (i = 0; i < ARRAY_SIZE(ctx->supplies); i++)
		ctx->supplies[i].supply = regulator_names[i];

	ret = devm_regulator_bulk_get(dev, ARRAY_SIZE(ctx->supplies),
				      ctx->supplies);
	if (ret < 0)
		return ret;

	ctx->reset_gpio = devm_gpiod_get(dev, "reset", GPIOD_OUT_HIGH);
	if (IS_ERR(ctx->reset_gpio)) {
		dev_err(dev, "cannot get reset-gpios %ld\n",
			PTR_ERR(ctx->reset_gpio));
		return PTR_ERR(ctx->reset_gpio);
	}

	ctx->mode_gpio = devm_gpiod_get(dev, "mode", GPIOD_OUT_LOW);
	if (IS_ERR(ctx->mode_gpio)) {
		dev_err(dev, "cannot get mode gpio %ld\n",
			PTR_ERR(ctx->mode_gpio));
		ctx->mode_gpio = NULL;
		return PTR_ERR(ctx->mode_gpio);
	}

	/* dual port */
	gpiod_set_value(ctx->mode_gpio, 0);

	ret = of_get_videomode(dev->of_node, &ctx->vm, 0);
	if (ret < 0)
		return ret;

	drm_panel_init(&ctx->panel);
	ctx->panel.dev = dev;
	ctx->panel.funcs = &truly_wqxga_drm_funcs;
	drm_panel_add(&ctx->panel);

	return 0;
}

static void truly_wqxga_panel_del(struct truly_wqxga *ctx)
{
	if (ctx->panel.dev)
		drm_panel_remove(&ctx->panel);
}

static int truly_wqxga_probe(struct mipi_dsi_device *dsi)
{
	struct device *dev = &dsi->dev;
	struct truly_wqxga *ctx;
	struct mipi_dsi_device *secondary;
	struct device_node *dsi1;
	struct mipi_dsi_host *dsi1_host;
	int ret = 0;


	printk(KERN_INFO "trulynt35521 : probe func \n");

	const struct mipi_dsi_device_info info = {
		.type = "trulynt35521",
		.channel = 0,
		.node = NULL,
	};

	ctx = devm_kzalloc(dev, sizeof(*ctx), GFP_KERNEL);

	if (!ctx) {
		ret = -ENOMEM;
		goto err_dsi_ctx;
	}

	/* This device represents itself as one with
	 * two input ports which are fed by the output
	 * ports of the two DSI controllers . The DSI0
	 * is the master controller and has most of the
	 * panel related info in its child node.
	 */

	dsi1 = of_graph_get_remote_node(dsi->dev.of_node, 1, -1);
	if (!dsi1) {
		dev_err(dev, "failed to get remote node for secondary DSI\n");
		ret = -ENODEV;
		goto err_get_remote;
	}

	dsi1_host = of_find_mipi_dsi_host_by_node(dsi1);
	if (!dsi1_host) {
		dev_err(dev, "failed to find dsi host\n");
		ret = -EPROBE_DEFER;
		goto err_host;
	}

	of_node_put(dsi1);

	/* register the second DSI device */
	secondary = mipi_dsi_device_register_full(dsi1_host, &info);

	if (IS_ERR(secondary)) {
		dev_err(dev, "failed to create dsi device\n");
		ret = PTR_ERR(dsi);
		goto err_dsi_device;
	}

	mipi_dsi_set_drvdata(dsi, ctx);

	ctx->dev = dev;
	ctx->dsi[0] = dsi;
	ctx->dsi[1] = secondary;

	ret = truly_wqxga_panel_add(ctx);
	if (ret) {
		dev_err(dev, "failed to add panel\n");
		goto err_panel_add;
	}

	/* configure master DSI device */
	dsi->lanes = 4;
	dsi->format = MIPI_DSI_FMT_RGB888;
	dsi->mode_flags = MIPI_DSI_MODE_VIDEO | MIPI_DSI_CLOCK_NON_CONTINUOUS |
						MIPI_DSI_MODE_LPM;

	ret = mipi_dsi_attach(dsi);
	if (ret < 0) {
		dev_err(dev, "master dsi attach failed\n");
		goto err_dsi_attach;
	}

	/* configure secondary DSI device */
	secondary->lanes = 4;
	secondary->format = MIPI_DSI_FMT_RGB888;
	secondary->mode_flags = MIPI_DSI_MODE_VIDEO |
		MIPI_DSI_CLOCK_NON_CONTINUOUS |
		MIPI_DSI_MODE_LPM;

	ret = mipi_dsi_attach(secondary);
	if (ret < 0) {
		dev_err(dev, "mipi_dsi_attach on secondary failed\n");
		goto err_dsi_attach_sec;
	}

	return 0;

	err_dsi_attach_sec:
		mipi_dsi_detach(ctx->dsi[0]);
	err_dsi_attach:
		truly_wqxga_panel_del(ctx);
	err_panel_add:
		mipi_dsi_device_unregister(secondary);
	err_dsi_device:
	err_host:
		of_node_put(dsi1);
	err_get_remote:
	err_dsi_ctx:
		return ret;
	}

static int truly_wqxga_remove(struct mipi_dsi_device *dsi)
{
	struct truly_wqxga *ctx = mipi_dsi_get_drvdata(dsi);

	if (ctx->dsi[0])
		mipi_dsi_detach(ctx->dsi[0]);
	if (ctx->dsi[1]) {
		mipi_dsi_detach(ctx->dsi[1]);
		mipi_dsi_device_unregister(ctx->dsi[1]);
	}
	truly_wqxga_panel_del(ctx);

	return 0;
}

static const struct of_device_id truly_wqxga_of_match[] = {
	{ .compatible = "truly,nt35521", },
	{ }
};
MODULE_DEVICE_TABLE(of, truly_wqxga_of_match);

static struct mipi_dsi_driver truly_wqxga_driver = {
	.driver = {
		.name = "panel_truly_nt35521",
		.of_match_table = truly_wqxga_of_match,
	},
	.probe = truly_wqxga_probe,
	.remove = truly_wqxga_remove,
};
module_mipi_dsi_driver(truly_wqxga_driver);

//MODULE_DESCRIPTION("Truly NT35597 DSI Panel Driver");
//MODULE_LICENSE("GPL v2");

MODULE_AUTHOR("Sijo K Saju <sijo.k@nymble.in>");
MODULE_DESCRIPTION("DRM Driver for NT35521 DSI Panel Driver ");
MODULE_LICENSE("GPL and additional rights");
