/*
 * Copyright (C) 2011, 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ANDROID_HPTA32X32_HAL_INTERFACE_H
#define ANDROID_HPTA32X32_HAL_INTERFACE_H

#include <stdint.h>
#include <strings.h>
#include <sys/cdefs.h>
#include <sys/types.h>
#include <hardware/hardware.h>
#include <linux/i2c.h>
#include <linux/i2c-dev.h>

__BEGIN_DECLS

 
/*
 *
 * Begin HPTA32X32 specific HAL
 */

#define true 1
#define false 0

#define DEVICE_NAME "/dev/i2c-2"
#define MODULE_NAME "Hpta"
#define MODULE_AUTHOR "sijo.k@nymble.in"

#define BUFF_LENGTH 258

#define THERMALCAMERA_HTPA32X32_CONTROLLER  "htpa32x32"
/*Define module ID*/
#define HPTA_HARDWARE_MODULE_ID "hpta"

/* Sensor Addr*/
#define ADDR			0x1A

/* use eeprom extracted data*/

/* wake up */
#define  CONFIG_REG 		0x01
#define  WAKEUP 			0x01
#define  START_WAKEUP		0x09
#define  STATUS_REG 		0x02

/* blocks */
#define BLOCK0_CONFIG		0x09
#define BLOCK1_CONFIG		0x19
#define BLOCK2_CONFIG		0x29
#define BLOCK3_CONFIG		0x39


/**Electrical Offsets*/
#define EL_OFFSETS_TOP_REG	0x0F		
#define EL_OFFSETS_BOOTOM_REG	0x0F


/* Address block*/
#define READ_BLOCK_TOP_REG	0x0A
#define READ_BLOCK_BOTTOM_REG	0x0B

/* calib reg*/
#define  TRIM_REG1		0x03
#define  MBIT_TRIM		0x2c

#define  TRIM_REG2		0x04
#define  BIAS_TRIML		0x05

#define  TRIM_REG3		0x05
#define  BIAS_TRIMR		0x05

#define TRIM_REG4		0x06
#define CLK_TRIM		0x15

#define TRIM_REG5		0x07
#define BPA_TRIML		0x0C

#define TRIM_REG6		0x08
#define BPA_TRIMR		0x0C

#define TRIM_REG7		0x09
#define PU_TRIM			0x88

#define SLEEP			0x00



 
/* hardware module structure */ 
struct hpta_module_t{
	 
	struct hw_module_t common; 

}; 

/*Hardware interface structure*/
struct hpta_device_t { 
	
	struct hw_device_t common; 
	int fd; 
	struct i2c_msg* msgs;
	int (*hpta_open)(struct hpta_device_t* dev);
	// int (*hpta_calibrate)(struct hpta_device_t* dev); 
	// int (*hpta_get_val)(struct hpta_device_t* dev,int * g_b);
	int (*i2c_transfer)(struct hpta_device_t* dev,struct i2c_msg* msgs, size_t count);

};


__END_DECLS

#endif // ANDROID_THERMALCAMERA_HTPA32X32_INTERFACE_H
