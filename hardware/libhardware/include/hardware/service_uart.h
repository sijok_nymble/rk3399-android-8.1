/*
 * Copyright (C) 2011, 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ANDROID_SERVICE_UART_INTERFACE_H
#define ANDROID_SERVICE_UART_INTERFACE_H

#include <stdint.h>
#include <strings.h>
#include <sys/cdefs.h>
#include <sys/types.h>
#include <hardware/hardware.h>

__BEGIN_DECLS


#define true 1
#define false 0

#define DEVICE_NAME "/dev/ttyS4"
#define MODULE_NAME "UART_SERVICE"
#define MODULE_AUTHOR "sijo.k@nymble.in"

#define BUFF_LENGTH 258

/*Define module ID*/
#define UART_HARDWARE_MODULE_ID "serviceuart"




/* hardware module structure */ 
struct uart_module_t{
	 
	struct hw_module_t common; 

}; 

/*Hardware interface structure*/
struct uart_device_t { 
	
	struct hw_device_t common; 
	int fd;
	int (*uart_open)(struct uart_device_t* device,const char *file ,int speed); 
	int (*uart_write)(struct uart_device_t* dev,char* write_buffer,int length);/* arg2 bytes stream write*/ 
	int (*uart_read)(struct uart_device_t* dev,char* read_buffer,int length);/* arg2 bytes stream read*/

};


__END_DECLS

#endif // ANDROID_SERVICE_UART_INTERFACE_H
