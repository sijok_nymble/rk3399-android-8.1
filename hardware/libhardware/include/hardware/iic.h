#ifndef _HARDWARE_IIC_H
#define _HARDWARE_IIC_H
 
 
#include <stdint.h>
#include <strings.h>
#include <sys/cdefs.h>
#include <sys/types.h>
 
#include <hardware/hardware.h>
 
__BEGIN_DECLS
 
#define IIC_HARDWARE_MODULE_ID "iic"
#define DEVICE_NAME "/dev/i2c-2"
 
#define I2C_RETRIES 0x0701
#define I2C_TIMEOUT 0x0702
#define I2C_RDWR	0x0707  
#define I2C_M_TEN 	0x0010  
#define I2C_M_RD 	0x0001
 
/* hardware module structure */ 
struct iic_module_t {
 
    struct hw_module_t common;
 
};
 
struct iic_device_t {
 
    struct hw_device_t common;
	int fd;
	int (*iic_open)(struct iic_device_t* dev,const char * pathStr);
	int (*iic_write)(struct iic_device_t *dev,unsigned int fd, unsigned int slaveAddr, unsigned int regAddr, unsigned char dataBuf);
    int (*iic_read)(struct iic_device_t *dev,unsigned int fd, unsigned int slaveAddr, unsigned int regAddr, unsigned char *dataBuf, uint16_t len);
 
}; 
 
 
// static inline int iic_open(const struct hw_module_t* module,iic_device_t** dev) {
//     return module->methods->open(module, IIC_HARDWARE_MODULE_ID,(struct hw_device_t**) dev);
// }
 
// static inline int iic_close(struct iic_device_t* dev) {
//     return dev->common.close(&dev->common);
// }

__END_DECLS
 
#endif
