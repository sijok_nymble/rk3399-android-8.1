/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define LOG_TAG "UART-SERV-Stub"
#include <hardware/hardware.h>
#include <hardware/service_uart.h>

//#include <linux/delay.h>

#include <errno.h>
#include <fcntl.h>
#include <malloc.h>
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <log/log.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <termios.h>
 

static int uart_close(struct hw_device_t* device) {

	struct uart_device_t* uart_device = (struct uart_device_t*)device;
	
	if(uart_device) {
		close(uart_device->fd);
		free(uart_device);
	}

    return 0;
}

static int uart_swrite(struct uart_device_t* device,char *buffer,int length)
{


	int ret = write(device->fd, buffer, length);
	if (ret < 0)
		__android_log_print(ANDROID_LOG_ERROR, "UART HAL", "%s", " write Array error . ");


    return ret;

}

static int uart_sread(struct uart_device_t* device,char *buffer,int length)
{


	int ret = read(device->fd,buffer,length);
	if (ret < 0)
		__android_log_print(ANDROID_LOG_ERROR, "UART HAL", "%s", " read Array error . ");

    return ret;

}

static int uart_sopen(struct uart_device_t* device,const char *file ,int speed)
{

	__android_log_print(ANDROID_LOG_ERROR, "UART-HAL", "%s", "device_exists call ");

	/*File Descriptor*/

	device->fd = open(file,O_RDWR | O_NOCTTY | O_NONBLOCK);	
			   						/* O_RDWR Read/Write access to serial port           */
									/* O_NOCTTY - No terminal will control the process   */
									/* O_NDELAY -Non Blocking Mode,Does not care about-  */
									/* -the status of DCD line,Open() returns immediatly */  
    if(device->fd < 0) {
		__android_log_print(ANDROID_LOG_ERROR, "UART-HAL", "%s", "device open failed ");
	close(device->fd);
        return false;
    }                                      
									
	switch (speed) {
        case 2400:
            speed = B2400;
            break;
        case 4800:
            speed = B4800;
            break;
        case 9600:
            speed = B9600;
            break;
        case 19200:
            speed = B19200;
            break;
        case 38400:
            speed = B38400;
            break;
        case 57600:
            speed = B57600;
            break;
        case 115200:
            speed = B115200;
            break;
        default:
        	speed = B9600;
            
    }
    struct termios tio;  						/* Create the structure                          */
    if (tcgetattr(device->fd, &tio))             		/* Get the current attributes of the Serial port */
        memset(&tio, 0, sizeof(tio));

    tio.c_cflag =  speed | CS8 | CLOCAL | CREAD;
    // Disable output processing, including messing with end-of-line characters.
    tio.c_oflag &= ~OPOST;
    tio.c_iflag = IGNPAR;
    tio.c_lflag = 0; /* turn of CANON, ECHO*, etc */
    /* no timeout but request at least one character per read */
    tio.c_cc[VTIME] = 1;
    tio.c_cc[VMIN] = 0;
    tcsetattr(device->fd, TCSANOW, &tio);
    tcflush(device->fd, TCIFLUSH);

 //    /*---------- Setting the Attributes of the serial port using termios structure --------- */
	// struct termios SerialPortSettings;

	// cfsetispeed(&SerialPortSettings,speed); /* Set Read  Speed                        */
	// cfsetospeed(&SerialPortSettings,speed); /* Set Write Speed                        */

	// SerialPortSettings.c_cflag &= ~PARENB;   /* Disables the Parity Enable bit(PARENB),So No Parity   */
	// SerialPortSettings.c_cflag &= ~CSTOPB;   /* CSTOPB = 2 Stop bits,here it is cleared so 1 Stop bit */
	// SerialPortSettings.c_cflag &= ~CSIZE;	 /* Clears the mask for setting the data size             */
	// SerialPortSettings.c_cflag |=  CS8;      /* Set the data bits = 8                                 */
	
	// SerialPortSettings.c_cflag &= ~CRTSCTS;   /*     No Hardware flow Control                       */  
	// SerialPortSettings.c_cflag |= CREAD | CLOCAL; /* Enable receiver,Ignore Modem Control lines       */ 
		
		
	// SerialPortSettings.c_iflag &= ~(IXON | IXOFF | IXANY);          /* Disable XON/XOFF flow control both i/p and o/p */
	// SerialPortSettings.c_iflag &= ~(ICANON | ECHO | ECHOE | ISIG);  /* Non Cannonical mode                            */
	// SerialPortSettings.c_oflag &= ~OPOST;/*No Output Processing*/

	// if((tcsetattr(device->fd,TCSANOW,&SerialPortSettings)) != 0) /* Set the attributes to the termios structure*/
	// 	    __android_log_print(ANDROID_LOG_ERROR, "UART-HAL", "%s"," ERROR ! in Setting attributes");
	// 	else
	// 	    __android_log_print(ANDROID_LOG_ERROR, "UART-HAL", "%s", "SUCCESS ! in Setting attributes");

    return	device->fd;
}

static struct uart_device_t uart_dev = {
	.common = {
		.tag   = HARDWARE_DEVICE_TAG,
		.close = uart_close,
		.version = 0,
	},
	.uart_open   = uart_sopen,
	.uart_write  = uart_swrite,
	.uart_read	 = uart_sread,

};

static int uart_device_open(const struct hw_module_t* module, const char* name, struct hw_device_t** device) {

	*device = &uart_dev;

    return 0;
}

/* module method table */
static struct hw_module_methods_t uart_module_methods = { 
	open: uart_device_open 
};

struct uart_module_t HAL_MODULE_INFO_SYM = {
     .common = {
         .tag = HARDWARE_MODULE_TAG,
         .version_major = 1,
         .version_minor = 0,
         .id = UART_HARDWARE_MODULE_ID,
         .name = MODULE_NAME,
         .author = MODULE_AUTHOR,
         .methods =  &uart_module_methods,
     },
 };
