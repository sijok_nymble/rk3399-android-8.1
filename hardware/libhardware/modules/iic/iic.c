
#include <hardware/iic.h>
#include <hardware/hardware.h>
 
#include <cutils/atomic.h>   
#include <linux/i2c.h>  
#include <linux/i2c-dev.h>  
#include <stdlib.h>  
#include <linux/types.h>   
#include <sys/types.h>  
#include <sys/ioctl.h> 
 
#include <cutils/log.h>
#include <malloc.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <math.h>
#include <string.h>

#define  LOG_TAG    "NymbleI2C"
#define  LOGI(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)
#define  LOGE(...)  __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)


struct i2c_rdwr_ioctl_data iic_data;
int ret;
 
static int i2c_write(struct iic_device_t *dev __unused,unsigned int fd, unsigned int slaveAddr, unsigned int regAddr, unsigned int dataBuf) 
   {

	 	 unsigned char msg[2] = " ";
	 	 msg[0]=regAddr;
	 	 msg[1]=dataBuf;

		 struct i2c_msg msgs[1] = {
		    { .addr = slaveAddr, 
		      .flags = 0,
		      .len = 2, 
		      .buf = msg
		    },

		  };


	    /** Prepare I2C transfer structure */
	    memset(&iic_data, 0, sizeof(struct i2c_rdwr_ioctl_data));
	    iic_data.msgs = msgs;
	    iic_data.nmsgs = 1;

		LOGI("IIC write HAL: From register %06x write... ",regAddr);
		LOGI("%04x ",(iic_data.msgs[0]).buf[1]);
	 
		ret=ioctl(fd,I2C_RDWR,&iic_data);
		if(ret<0){ 
			
			LOGE("IIC HAL write ioctl error....");
			return -1;
		}
		
		usleep(3000);
		return 0;
    }
 
 
static int i2c_read(struct iic_device_t *dev __unused,unsigned int fd, unsigned int slaveAddr, unsigned int regAddr, unsigned char *dataBuf, uint16_t len) {   
 

	  unsigned char msg_addr[1] =" ";
	  // unsigned char msg_data[258] =dataBuf;
	  
	  msg_addr[0]=regAddr;
	  struct i2c_msg msgs[2] = {
		    { .addr = slaveAddr, .flags = 0, .len = 1, .buf = msg_addr},
		    { .addr = slaveAddr,
		      .flags = I2C_M_RD,
		      .len = len,
		      .buf = dataBuf},
	  	};


		     /** Prepare I2C transfer structure */
	    memset(&iic_data, 0, sizeof(struct i2c_rdwr_ioctl_data));
	    iic_data.msgs = msgs;
	    iic_data.nmsgs = 2;


		LOGE("IIC read HAL: From register %06x read ",regAddr);
		LOGE("%04x ",(iic_data.msgs[1]).buf[0]);
	 
		ret=ioctl(fd,I2C_RDWR,&iic_data); 
		if(ret<0){
			LOGE("IIC HAL read ioctl error..%d",ret);
	 	}
		usleep(3000);
		return 0;
}
 
static int iic_close(struct hw_device_t *dev) {
    free(dev);
    return 0;
}

/* check the device functionality*/
static int i2c_dev_open(struct iic_device_t* device,const char * pathStr){

	__android_log_print(ANDROID_LOG_ERROR, "I2C-HAL", "%s", "device_exists call ");

 //    unsigned long supported_funcs; //TODO upward supported function
 //    device->fd = TEMP_FAILURE_RETRY(open(file, O_RDWR));
 //    if(device->fd < 0) {
	// 	__android_log_print(ANDROID_LOG_ERROR, "I2C-HAL", "%s", "device open failed ");
	// close(device->fd);
 //        return false;

	if((device->fd = open(pathStr, O_RDWR)) == -1) {
		LOGE("I2C-HAL: failed to open /dev/i2c-2 --");  
		free(device);	  
		return -EFAULT;    
	}else{		
		ALOGI("I2C-HAL: open %s successfully.",pathStr);
		iic_data.nmsgs=2;   
		iic_data.msgs=(struct i2c_msg*)malloc(iic_data.nmsgs*sizeof(struct i2c_msg));   
		if(!iic_data.msgs){
	       	LOGE("I2C-HAL :malloc error.");
			close(device->fd);  
	       	exit(1);  
	   	}
		ret=ioctl(device->fd, I2C_TIMEOUT, 2);  
		if(ret<0){ LOGE("IIC HAL I2C_TIMEOUT ioctl error.");}
	   	ret=ioctl(device->fd, I2C_RETRIES, 1);
	   	if(ret<0){ LOGE("IIC HAL I2C_RETRIES ioctl error.");}
	}
    return device->fd;
 }

static struct iic_device_t iic_dev = {
	.common = {
		.tag   = HARDWARE_DEVICE_TAG,
		.close = iic_close,
		.version = 0,
	},
	.iic_open  	 =  i2c_dev_open,
	.iic_write	 =  i2c_write,
	.iic_read    =  i2c_read,

};


static int iic_device_open(const struct hw_module_t* module, const char* name, struct hw_device_t** device) {

	*device = &iic_dev;

return 0;
}
 
/*===========================================================================*/
/* Default iic HW module interface definition                           */
/*===========================================================================*/
 
static struct hw_module_methods_t iic_module_methods = {
  .open = iic_device_open,
};


struct iic_module_t HAL_MODULE_INFO_SYM = {
  .common = {
	  .tag = HARDWARE_MODULE_TAG,
	  .module_api_version = 0x0100, // [15:8] major, [7:0] minor (1.0)
	  .hal_api_version = 0x00, // 0 is only valid value
	  .id = IIC_HARDWARE_MODULE_ID,
	  .name = "Nymble IIC HW HAL",
	  .author = "sijo.k@nymble.in",
	  .methods = &iic_module_methods,
  },
};
