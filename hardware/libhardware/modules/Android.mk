hardware_modules := \
    audio_remote_submix \
    camera \
    gralloc \
    hwcomposer \
    input \
    radio \
    sensors \
    thermal \
    usbaudio \
    usbcamera \
    vehicle \
    hpta32x32 \
    service_uart \
    vr \
    iic
include $(call all-named-subdir-makefiles,$(hardware_modules))
