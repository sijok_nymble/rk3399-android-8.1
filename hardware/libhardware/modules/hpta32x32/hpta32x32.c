/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define LOG_TAG "HptaStub"
#include <hardware/hardware.h>
#include <hardware/hpta32x32.h>

//#include <linux/delay.h>

#include <errno.h>
#include <fcntl.h>
#include <malloc.h>
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <log/log.h>

#define  LOG_TAG    "NymbleI2C"
#define  LOGI(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)
#define  LOGE(...)  __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)


//static int g_b[2580];


static int hpta_close(struct hw_device_t* device) {
	struct hpta_device_t* hpta_device = (struct hpta_device_t*)device;
	if(hpta_device) {
		close(hpta_device->fd);
		free(hpta_device);
	}

 return 0;
}

static int i2c_transfer(struct hpta_device_t* dev, struct i2c_msg* msgs, size_t count){

	struct i2c_rdwr_ioctl_data i2c_rdwr_data;
    int ret;

    /** Prepare I2C transfer structure */
    memset(&i2c_rdwr_data, 0, sizeof(struct i2c_rdwr_ioctl_data));
    i2c_rdwr_data.msgs = msgs;
    i2c_rdwr_data.nmsgs = count;

    /** Transfer */
    ret=ioctl(dev->fd, I2C_RDWR, &i2c_rdwr_data); 
    if(ret< 0){
			__android_log_print(ANDROID_LOG_ERROR, "HPTA-HAL Test", "%s-err-%d -","TestFF",ret);

					return -5;
    }
    //#endif


 return 1;
}





// static int hpta_get_val(struct hpta_device_t* dev,int * g_b) {
// 	/*if(!dev) {
// 		//LOGE("Hpta Stub: error val pointer");
// 		return -EFAULT;
// 	} */
// 	//int g_b[2580];
// 	__android_log_print(ANDROID_LOG_ERROR, "HPTA-HAL Test", "%s-","Test1");

// 	int j = 0;
// 	uint8_t msg_addr_blk[2] = { CONFIG_REG, START_WAKEUP };			//starting the thermal reading
// 	struct i2c_msg msgs_blk[1] = {
// 		{ .addr = ADDR, .flags = 0, .len = 2, .buf = (char*)msg_addr_blk },
// 	};
// 	if(i2c_transfer(dev->fd, msgs_blk, 1) < 0) {

// 				return false;
// 	}

// 	usleep(30000);

// 	uint8_t msg_addr_status[1] = { STATUS_REG };
// 	uint8_t msg_data_status[1] = {};
// 	struct i2c_msg msgs_status[2] = {
// 		{ .addr = ADDR,
// 			.flags = 0,
// 			.len = 1,
// 			.buf = (char*)msg_addr_status },

// 		/** Read 8-bit data */
// 		{ .addr = ADDR,
// 			.flags = I2C_M_RD,
// 			.len = 1,
// 			.buf = (char*)msg_data_status },
// 	};

// 	/** Transfer a transaction with two I2C messages */
// 	if(i2c_transfer(dev, msgs_status, 2) < 0) {
// 		return false;
// 	}

// 		/**Reading top block*//**Reading sensor values Block 0*/
// 	uint8_t msg_addr_read[1] = { READ_BLOCK_TOP_REG };
// 	uint8_t msg_data_read[258] = {};
// 	struct i2c_msg msgs_read[2] = {
// 		{ .addr = ADDR, .flags = 0, .len = 1, .buf = (char*)msg_addr_read },
// 		{ .addr = ADDR,
// 			.flags = I2C_M_RD,
// 			.len = 258,
// 			.buf = (char*)msg_data_read },
// 	};

// 	if(i2c_transfer(dev->fd, msgs_read, 2) < 0) {
// 		return false;
// 	}
// 	for (int i = 0; i < 258; i++, j++)
// 	{
// 		g_b[j] = msg_data_read[i];
// 		//__android_log_print(ANDROID_LOG_ERROR, "", "%d-",g_b[1]);

// 	}

// 		/**Rading bottom block*/
// 	msg_addr_read[0] = READ_BLOCK_BOTTOM_REG;
// 	/** Transfer a transaction with two I2C messages */
// 	if(i2c_transfer(dev->fd, msgs_read, 2) < 0) {
// 		return false;
// 	}
// 	for (int i = 0; i < 258; i++, j++){
// 		g_b[j] = msg_data_read[i];
// 	}
// 									/**Reading sensor values Block 1*/
// 	msg_addr_blk[1] = BLOCK1_CONFIG;
// 	if(i2c_transfer(dev->fd, msgs_blk, 1) < 0) {
// 		return false;
// 	}
// 	usleep(30000);

// 	msg_addr_status[0] = STATUS_REG;
// 	/** Transfer a transaction with two I2C messages */
// 	if(i2c_transfer(dev->fd, msgs_status, 2) < 0) {
// 		return false;
// 	}

// 	/**Reading top block*/
// 	msg_addr_read[0] = READ_BLOCK_TOP_REG;
// 	if(i2c_transfer(dev->fd, msgs_read, 2) < 0) {
// 		return false;
// 	}

// 	for (int i = 0; i < 258; i++, j++){
// 		g_b[j] = msg_data_read[i];
// 	}

// 	/**Rading bottom block*/
// 	msg_addr_read[0] = READ_BLOCK_BOTTOM_REG;
// 	/** Transfer a transaction with two I2C messages */
// 	if(i2c_transfer(dev->fd, msgs_read, 2) < 0) {
// 		return false;
// 	}
// 	for (int i = 0; i < 258; i++, j++){
// 		g_b[j] = msg_data_read[i];
// 	}
// 									/**Reading sensor values Block 2*/
// 	msg_addr_blk[1] = BLOCK2_CONFIG;
// 	if(i2c_transfer(dev->fd, msgs_blk, 1) < 0) {
// 		return false;
// 	}
// 	usleep(30000);

// 	msg_addr_status[0] = STATUS_REG;
// 	/** Transfer a transaction with two I2C messages */
// 	if(i2c_transfer(dev->fd, msgs_status, 2) < 0) {
// 		return false;
// 	}

// 	/**Reading top block*/
// 	msg_addr_read[0] = READ_BLOCK_TOP_REG;
// 	if(i2c_transfer(dev->fd, msgs_read, 2) < 0) {
// 		return false;
// 	}
// 	for (int i = 0; i < 258; i++, j++){
// 		g_b[j] = msg_data_read[i];
// 	}
// 	/**Rading bottom block*/
// 	msg_addr_read[0] = READ_BLOCK_BOTTOM_REG;
// 	/** Transfer a transaction with two I2C messages */
// 	if(i2c_transfer(dev->fd, msgs_read, 2) < 0) {
// 		return false;
// 	}
// 	for (int i = 0; i < 258; i++, j++){
// 		g_b[j] = msg_data_read[i];
// 	}
// 									/**Reading sensor values Block 3*/
// 	msg_addr_blk[1] = BLOCK3_CONFIG;
// 	if(i2c_transfer(dev->fd, msgs_blk, 1) < 0) {
// 		return false;
// 	}
// 	usleep(30000);

// 	msg_addr_status[0] = STATUS_REG;
// 	/** Transfer a transaction with two I2C messages */
// 	if(i2c_transfer(dev->fd, msgs_status, 2) < 0) {
// 		return false;
// 		}

// 	/**Reading top block*/
// 	msg_addr_read[0] = READ_BLOCK_TOP_REG;
// 	if(i2c_transfer(dev->fd, msgs_read, 2) < 0) {
// 		return false;
// 	}

// 	for (int i = 0; i < 258; i++, j++){
// 		g_b[j] = msg_data_read[i];
// 	}
// 	/**Rading bottom block*/
// 	msg_addr_read[0] = READ_BLOCK_BOTTOM_REG;
// 	/** Transfer a transaction with two I2C messages */
// 	if(i2c_transfer(dev->fd, msgs_read, 2) < 0) {
// 		return false;
// 	}

// 	for (int i = 0; i < 258; i++, j++){
// 		g_b[j] = msg_data_read[i];
// 	}


// 	/**Electrical Offsets*/
// 	msg_addr_blk[1] = EL_OFFSETS_TOP_REG;
// 	if(i2c_transfer(dev->fd, msgs_blk, 1) < 0) {
// 		return false;
// 	}
// 	usleep(30000);

// 	msg_addr_status[0] = STATUS_REG;
// 	/** Transfer a transaction with two I2C messages */
// 	if(i2c_transfer(dev->fd, msgs_status, 2) < 0) {
// 		return false;
// 	}

// 	/**Reading top block*/
// 	msg_addr_read[0] = READ_BLOCK_TOP_REG;
// 	if(i2c_transfer(dev->fd, msgs_read, 2) < 0) {
// 		return false;
// 	}
// 	for (int i = 0; i < 258; i++, j++)
// 	{
// 		g_b[j] = msg_data_read[i];
// 	}
// 	/**Rading bottom block*/
// 	msg_addr_read[0] = READ_BLOCK_BOTTOM_REG;
// 	/** Transfer a transaction with two I2C messages */
// 	if(i2c_transfer(dev->fd, msgs_read, 2) < 0) {
// 		return false;
// 	}

// 	for (int i = 0; i < 258; i++, j++){
// 		g_b[j] = msg_data_read[i];
// 	}


//    //__android_log_print(ANDROID_LOG_ERROR, "", "%d-",g_b[155]);


//    return 1;
// }

static bool device_exists(const char *file ,struct hpta_device_t* device) {

	__android_log_print(ANDROID_LOG_ERROR, "HPTA-HAL", "%s", "device_exists call ");

    unsigned long supported_funcs; //TODO upward supported function
    device->fd = TEMP_FAILURE_RETRY(open(file, O_RDWR));
    if(device->fd < 0) {
		__android_log_print(ANDROID_LOG_ERROR, "HPTA-HAL", "%s", "device open failed ");
	close(device->fd);
        return false;
    }




    //if (ioctl(device->fd, I2C_FUNCS, &supported_funcs) < 0) {
       //close(device->fd);
        //return false;
    //}

    //if (!(supported_funcs & I2C_FUNC_I2C)) {
		//__android_log_print(ANDROID_LOG_ERROR, "HPTA-HAL", "%s", "failed supported_funcs call ");
        //close(device->fd);
        //return false;
    //}

    return true;
}

/* check the device functionality*/
static int hpta_open(struct hpta_device_t* devfd){
 return device_exists(DEVICE_NAME,devfd);
}

// static int hpta_calibrate(struct hpta_device_t* dev) 
// {

// 	//__android_log_print(ANDROID_LOG_ERROR, "HPTA-HAL", "%s", "device calibrate call ");

// 	uint8_t msg_addr_mbit[2] = { TRIM_REG1, MBIT_TRIM };
// 	struct i2c_msg msgscalib[1] = {
// 		{ .addr = ADDR, .flags = 0, .len = 2, .buf = (char*)msg_addr_mbit },

// 	};

// 	if(i2c_transfer(dev->fd, msgscalib, 1) < 0) {
// 		return false;
// 	}
// 	usleep(5000);
// 	__android_log_print(ANDROID_LOG_ERROR, "HPTA-HAL Test", "%s-","TestS1");
// 	uint8_t msg_addr_bias1[2] = { TRIM_REG2, BIAS_TRIML };
// 	msgscalib->buf = (char*)msg_addr_bias1;
// 	if(i2c_transfer(dev->fd, msgscalib, 1) < 0) {
// 		return false;
// 	}
// 	usleep(5000);
// 	__android_log_print(ANDROID_LOG_ERROR, "HPTA-HAL Test", "%s-","TestS2");
// 	uint8_t msg_addr_bias2[2] = { TRIM_REG3, BIAS_TRIMR };
// 	msgscalib->buf = (char*)msg_addr_bias2;
// 	if(i2c_transfer(dev->fd, msgscalib, 1) < 0) {
// 		return false;
// 	}
// 	usleep(5000);
// 	__android_log_print(ANDROID_LOG_ERROR, "HPTA-HAL Test", "%s-","TestS3");
// 	uint8_t msg_addr_clk[2] = { TRIM_REG4, CLK_TRIM };
// 	msgscalib->buf = (char*)msg_addr_clk;
// 	if(i2c_transfer(dev->fd, msgscalib, 1) < 0) {
// 		return false;
// 	}
// 	usleep(5000);
// 	__android_log_print(ANDROID_LOG_ERROR, "HPTA-HAL Test", "%s-","TestS4");
// 	uint8_t msg_addr_bpa1[2] = { TRIM_REG5, BPA_TRIML };
// 	msgscalib->buf = (char*)msg_addr_bpa1;
// 	if(i2c_transfer(dev->fd, msgscalib, 1) < 0) {
// 		return false;
// 	}
// 	usleep(5000);
// 	__android_log_print(ANDROID_LOG_ERROR, "HPTA-HAL Test", "%s-","TestS5");
// 	uint8_t msg_addr_bpa2[2] = { TRIM_REG6, BPA_TRIMR };
// 	msgscalib->buf = (char*)msg_addr_bpa2;
// 	if(i2c_transfer(dev->fd, msgscalib, 1) < 0) {
// 		return false;
//     }
// 	usleep(5000);
// 	__android_log_print(ANDROID_LOG_ERROR, "HPTA-HAL Test", "%s-","TestS6");
// 	uint8_t msg_addr_pu[2] = { TRIM_REG7, PU_TRIM };
// 	msgscalib->buf = (char*)msg_addr_pu;
// 	if(i2c_transfer(dev->fd, msgscalib, 1) < 0) {
// 		return false;
// 	}

// 	__android_log_print(ANDROID_LOG_ERROR, "HPTA-HAL Test", "%s-","TestS7");

// 	return true;
// }

static struct hpta_device_t hpta_dev = {
	.common = {
		.tag   = HARDWARE_DEVICE_TAG,
		.close = hpta_close,
		.version = 0,
	},
	.hpta_open  	 = hpta_open,
	//.hpta_calibrate  = hpta_calibrate,
	//.hpta_get_val	 = hpta_get_val,
	.i2c_transfer	 =i2c_transfer,

};

static int hpta_device_open(const struct hw_module_t* module, const char* name, struct hw_device_t** device) {

	*device = &hpta_dev;

return 0;
}

/* module method table */
static struct hw_module_methods_t hpta_module_methods = {
	open: hpta_device_open
};

struct hpta_module_t HAL_MODULE_INFO_SYM = {
     .common = {
         .tag = HARDWARE_MODULE_TAG,
         .version_major = 1,
         .version_minor = 0,
         .id = HPTA_HARDWARE_MODULE_ID,
         .name = MODULE_NAME,
         .author = MODULE_AUTHOR,
         .methods =  &hpta_module_methods,
     },
 };
