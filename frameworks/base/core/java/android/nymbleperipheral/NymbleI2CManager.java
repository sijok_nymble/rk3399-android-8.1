
package android.nymbleperipheral;


import android.os.RemoteException;
import android.annotation.SystemService;
import android.content.Context;
import java.io.IOException;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;




@SystemService(Context.NYMBLE_I2C_SERVICE)
public final class NymbleI2CManager{
    private static final String TAG = "NymbleI2cManager";

    private final Context mContext;
    private final INymbleI2CManager mService;


    public NymbleI2CManager(Context context, INymbleI2CManager service) {
        mContext = context;
        mService = service;
        
    }

    public int open(String pathName)
    {
        int fh=0;
        try{
           fh= mService.open(pathName);
        }catch(RemoteException e){
            //throw e.rethrowFromSystemServer();
            //throw new Exception("Device not opened!");
             e.printStackTrace();
        }
        return fh;
    }

    public int write(int fileHander,int slaveAddr,int regAddr, int buffdata, int len)
    {
        int st=0;
      try {
            st=   mService.write(fileHander,slaveAddr,regAddr,buffdata,len);
        } catch (RemoteException e) {
            //throw e.rethrowFromSystemServer();
            //throw new Exception("I2C write error!");
             e.printStackTrace();
         }
        return st;
    }


    public int read (int fileHander,int slaveAddr,int regAddr, int[] bufArr, int len)
    {
        
        try {
            return mService.read(fileHander,slaveAddr,regAddr,bufArr,len);

        } catch (RemoteException e) {
            //throw e.rethrowFromSystemServer();
            //throw new Exception("I2C read error!");

             e.printStackTrace();

        }
           
     return 0;      
    }

    public void close (int slaveAddr)
    {
        
        try {
            mService.close(slaveAddr);

        } catch (RemoteException e) {
            //throw e.rethrowFromSystemServer();
            //throw new Exception("I2C read error!");

             e.printStackTrace();

        }     
    }



    

}
