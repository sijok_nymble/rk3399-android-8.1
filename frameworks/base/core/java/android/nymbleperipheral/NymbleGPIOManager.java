
package android.nymbleperipheral;


import android.os.RemoteException;
import android.annotation.SystemService;
import android.content.Context;
import java.io.IOException;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;



@SystemService(Context.NYMBLE_GPIO_SERVICE)
public final class NymbleGPIOManager{
    private static final String TAG = "NymbleGPIOManager";

    private final Context mContext;
    private final INymbleGPIOManager mService;


    public NymbleGPIOManager(Context context, INymbleGPIOManager service) {
        mContext = context;
        mService = service;
    }


    public int setmcustate(int mcu ,int state)
    {
        try{
            return mService.setmcustate(mcu,state);
        }catch(RemoteException e){

            e.printStackTrace();
        }
        return -1;
    }


}
