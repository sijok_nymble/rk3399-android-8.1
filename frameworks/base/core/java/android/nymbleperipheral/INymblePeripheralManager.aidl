/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.nymbleperipheral;

import android.os.ParcelFileDescriptor;
import android.nymbleperipheral.ByteBuffer;

/** @hide */
interface INymblePeripheralManager
{
    /* Returns a list of all available serial ports */
    String[] getSerialPorts();

    /* Returns a file descriptor for the serial port. */
    ParcelFileDescriptor openSerialPort(String name);

    /* terminos setting for the port*/
    void portOpen(in ParcelFileDescriptor pfd, int speed);


    /* Returns bytes read from the serial port */
    int uartRead(out byte[] buffer,int length);

    /* Returns bytes written in the serial port */
    void uartWrite(in byte[] buffer, int length);

   /*  ReleaseUartDevice */
    void uartclose();

  

}
