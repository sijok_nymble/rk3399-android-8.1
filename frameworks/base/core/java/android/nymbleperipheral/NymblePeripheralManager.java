
package android.nymbleperipheral;


import android.os.RemoteException;
import android.annotation.SystemService;
import android.content.Context;
import java.io.IOException;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;

// import android.nymbleperipheral.NymbleSerialPort;
import java.nio.ByteBuffer;


@SystemService(Context.NYMBLE_PERIPHERAL_SERVICE)
public final class NymblePeripheralManager{
    private static final String TAG = "NymblePeripheralManager";

    private final Context mContext;
    private final INymblePeripheralManager mService;


    public NymblePeripheralManager(Context context, INymblePeripheralManager service) {
        mContext = context;
        mService = service;
    }

    /**
     * Returns a string array containing the names of available serial ports
     *
     * @return names of available serial ports
     */
    public String[] getSerialPorts()
    {
        try {
            return mService.getSerialPorts();
        } catch (RemoteException e) {
            throw e.rethrowFromSystemServer();
        }
    }

    /**
     * Opens and returns  with the given name.
     * The speed of the serial port must be one of:
     * 2400, 4800, 9600,
     * 19200, 38400, 57600, 115200, 230400, 460800, 500000, 576000, 921600, 1000000, 1152000,
     * 1500000
     *
     * @param name of the serial port
     * @param speed at which to open the serial port
     * @return the serial port fd
     */
    public ParcelFileDescriptor openSerialPort(String name, int speed)throws IOException
    {
        try {
            ParcelFileDescriptor pfd = mService.openSerialPort(name);
            if (pfd != null) {
                // NymbleSerialPort Serilport = new NymbleSerialPort(name);
                mService.portOpen(pfd, speed);
                // Serilport.open(pfd, speed);
              //String portname=port.getName();
                return pfd;
              } else {
                throw new IOException("Could not open serial port " + name);
            }
        } catch (RemoteException e) {
            throw e.rethrowFromSystemServer();
        }
    }

    /**
     * Reads data into the provided buffer.
     * Note that the value returned by {@link java.nio.Buffer#position()} on this buffer is
     * unchanged after a call to this method.
     *
     * @param buffer to read into
     * @return number of bytes read
     */

    public int uartRead(byte[] buffer,int length)
    {
        int byteread=0;
        try {
             byteread= mService.uartRead(buffer,length);

        } catch (RemoteException e) {
            throw e.rethrowFromSystemServer();
        }

        return byteread;
    }

    /**
     * Write and returns  with the written bytes size.
     *
     *
     * @param buffer the data need to send
     * @param length is size of the data packet
     * @return actual bytes written
     */

    public void uartWrite(byte[] buffer, int length)
    {

        try {


            mService.uartWrite(buffer,length);

        } catch (RemoteException e) {
            throw e.rethrowFromSystemServer();
        }

    }

    /**
     * Closes the serial port
     */
    public void closeuart() throws IOException {

        try{
          mService.uartclose();
        }catch(RemoteException e) {
            throw e.rethrowFromSystemServer();
        }


    }


}
