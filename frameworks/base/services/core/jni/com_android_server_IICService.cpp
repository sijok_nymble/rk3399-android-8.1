#include "jni.h"
#include "JNIHelp.h"
#include "android_runtime/AndroidRuntime.h"
#include <utils/misc.h>
#include <cutils/log.h>
#include <hardware/hardware.h>
#include <hardware/iic.h>
#include <stdio.h>

namespace android
{

	//struct iic_device *iic_dev = NULL;
    static struct iic_device_t* iic_device;
    static hw_module_t* module;
    static hw_device_t* device;

    static void iic_setVal(JNIEnv* env, jobject clazz, jint slaveAddr, jint regAddr, jint databuf) {

		if(!iic_device) {
			ALOGE("iic JNI: device is not open.");
            jniThrowException(env, "java/io/IOException", "device not opened");
		}

		iic_device->iic_write(iic_device,slaveAddr,regAddr,databuf);
    }

    static jint iic_getVal(JNIEnv* env, jobject clazz,jint slaveAddr, jint regAddr) {

		unsigned char data[1] = {0};
    	iic_device->iic_read(iic_device,slaveAddr,regAddr,data);

		if(!iic_device) {
            ALOGE("iic JNI: device is not open.");
            jniThrowException(env, "java/io/IOException", "device not opened");
		}
		return data[0];
    }

    static void iic_close(JNIEnv* env){

        if(iic_device) {
            free(iic_device);
        }
    }


    /* * Load the specified hardware abstraction layer module by hardware module ID and turn on the hardware * /*/
    static jboolean iic_init(JNIEnv* env, jclass clazz,const char * StringPath) {
        int err;
        __android_log_print(ANDROID_LOG_ERROR, "I2C-SERVICE-JNI", "%s", "1. native deviceOpen ... ");

        /* 1. hw_get_module */
        err = hw_get_module(IIC_HARDWARE_MODULE_ID, (hw_module_t const**)&module);
        __android_log_print(ANDROID_LOG_ERROR, "I2C-SERVICE-JNI device Open error", "%d",err); 
        //printf("device Open error ..\n");
        if (err == 0) {
            /* 2. get device : module->methods->open */

            err = module->methods->open(module,IIC_HARDWARE_MODULE_ID, &device);
           __android_log_print(ANDROID_LOG_ERROR, "I2C-SERVICE-JNI", "%s", "2. get device ... ");
            if (err == 0) {
                /* 3. call dev_open */
                __android_log_print(ANDROID_LOG_ERROR, "I2C-SERVICE-JNI", "%s", "3. call dev open ... ");
                iic_device = (struct iic_device_t *)device;
 
                return iic_device->iic_open(iic_device,"/dev/i2c-2");
            } else {
            __android_log_print(ANDROID_LOG_ERROR, "I2C-SERVICE-JNI", "%s", "error dev open ... ");
                return -1;
            }
        }
        return -1;
  //       ------------------------------------------------------

 	// 	iic_module_t *module;
		// int err;
		// err = hw_get_module(IIC_HARDWARE_MODULE_ID,(const struct hw_module_t**)&module);
		// if (err != 0) {
		// 	ALOGE("Error acquiring iic hardware module: %d", err);
		// 	return 0;
		// }

		// err = iic_open(&(module->common), &iic_dev);
		// if (err != 0) {
		// 	ALOGE("Error opening iic hardware module: %d", err);
		// 	return 0;
		// }
        //       ALOGE("iic device is opening...");
        //       return 1;
    }
        /*JNI Method Table*/
    static const JNINativeMethod method_table[] = {
        {"init_native", "(Ljava/lang/String;)Z", (void*)iic_init},
        {"setVal_native", "(III)V", (void*)iic_setVal},
        {"getVal_native", "(II)I", (void*)iic_getVal},
        {"native_close",   "()V" ,(void *)iic_close},
    };
    /*Register JNI method*/
    int register_android_server_IICService(JNIEnv *env) {
            return jniRegisterNativeMethods(env, "com/android/server/nymbleperipheral/NymbleI2CService", method_table, NELEM(method_table));
        //    com/android/server/nymbleperipheral/NymbleI2CService"
    }
};
