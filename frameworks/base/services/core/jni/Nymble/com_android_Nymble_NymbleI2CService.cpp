/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "jni.h"
#include "JNIHelp.h"
#include "android_runtime/AndroidRuntime.h"
#include <utils/misc.h>
#include <cutils/log.h>
#include <hardware/hardware.h>
#include <hardware/iic.h>
#include <stdio.h>

#include <linux/i2c.h>
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>

namespace android
{
    static struct iic_device_t* iic_device;
    static hw_module_t* module;
    static hw_device_t* device;

      /* open the device*/
    static int iic_device_open(const char * PathString)
    {
            int err;

                // printf("native deviceOpen ...\n");
                __android_log_print(ANDROID_LOG_ERROR, "IIC-SERVICE-JNI", "%s", "1. native deviceOpen ... ");
                //printf("device Open .\n");

                  /* 1. hw_get_module */
                  err = hw_get_module(IIC_HARDWARE_MODULE_ID, (hw_module_t const**)&module);
                // __android_log_print(ANDROID_LOG_ERROR, "IIC-SERVICE-JNI device Open error", "%d",err); 
              //printf("device Open error ..\n");
                  if (err == 0) {
                      /* 2. get device : module->methods->open */
                      err = module->methods->open(module, IIC_HARDWARE_MODULE_ID, &device);
                      // __android_log_print(ANDROID_LOG_ERROR, "IIC-SERVICE-JNI", "%s", "2. get device ... ");
                //printf("2. get device ... \n");
                      if (err == 0) {
                          /* 3. call dev_open */
                          // __android_log_print(ANDROID_LOG_ERROR, "IIC-SERVICE-JNI", "%s", "3. call dev open ... ");
                        //printf("3. call dev open .... \n");
                          iic_device = (struct iic_device_t *)device;
           
                          return iic_device->iic_open(iic_device,PathString);
                      } else {
                      // __android_log_print(ANDROID_LOG_ERROR, "ICC-SERVICE-JNI", "%s", "error dev open ... ");
                      //  printf( "error dev open ... ");
                          return -1;
                      }
                  }
                  return -1;
     }

    static jint openI2C(JNIEnv *env, jobject obj, jstring file)
      {
 
           /* File descriptor to i2c-dev */
           int i2c_fd;

           /* Get device file name */
           const char *pathStr = env->GetStringUTFChars(file, NULL);
           if (pathStr == NULL)
            {
                __android_log_print(ANDROID_LOG_ERROR, "I2C-HAL", "%s", "Can't get file name!");

             return -1;
            }

           /* Get the device file name */
           // sprintf(fileName, "%s", str);
           //LOGI("Open i2c device node %s", fileName);
           __android_log_print(ANDROID_LOG_INFO, "I2C-HAL :", "Open i2c device node %s",pathStr);
           
           i2c_fd = iic_device_open(pathStr);
           if (i2c_fd < 0) {
            // ALOGE("could not open %s", pathStr);
            __android_log_print(ANDROID_LOG_INFO, "I2C-HAL :","could not open %s", pathStr);
            env->ReleaseStringUTFChars(file, pathStr);
            return -1;
            }
            env->ReleaseStringUTFChars(file, pathStr);

           return (jint) i2c_fd;
       }


    //***************************************************************************
    // Read data from the I2C device
    //***************************************************************************
      
    static jint  readI2C(JNIEnv * env, jobject obj, jint fileHander, jint slaveAddr,jint regAddr, jintArray bufArr, jint len)
    {
        jint *bufInt;
        unsigned char *bufByte;
        int res = 0, i = 0, j = 0;
        
        if (len <= 0) {
              __android_log_print(ANDROID_LOG_ERROR, "IIC-SERVICE-JNI", "%s", "Can't get file name!");
    
            return -1;
        }
          
        bufInt = (jint *) malloc(len * sizeof(int));
        if (bufInt == 0) {
           // LOGE("I2C: nomem");
            __android_log_print(ANDROID_LOG_ERROR, "IIC-SERVICE-JNI", "%s", "nomem");

            return -1;
        }
        bufByte = (unsigned char*) malloc(len);
        if (bufByte == 0) {
            // LOGE("I2C: nomem");
            __android_log_print(ANDROID_LOG_ERROR, "IIC-SERVICE-JNI", "%s", "nomem");

            free(bufInt);
            return -1;
        }
        
        env->GetIntArrayRegion(bufArr, 0, len, bufInt);
 
        // res = ioctl(fileHander,I2C_SLAVE, slaveAddr);
        // if (res != 0) {
        //     // LOGE("I2C: Can't set slave address");
        //     __android_log_print(ANDROID_LOG_ERROR, "I2C-HAL", "%s", "Can't set slave address");

        //     free(bufByte);
        //     free(bufInt);
        //     return -1;
        // }

        memset(bufByte, '\0', len);
        res=iic_device->iic_read(iic_device,(int)fileHander,slaveAddr,regAddr,bufByte, len);

        
        if (res<0) {
            // LOGE("read fail in i2c read jni i = %d buf 4", i);
            __android_log_print(ANDROID_LOG_ERROR, "IIC-SERVICE-JNI", "read fail in i2c read jni i = %d buf 4",i);

            free(bufByte);
            free(bufInt);
            return -1;    
        } else {
        for (i = 0; i < len ; i++)
            bufInt[i] = bufByte[i];
            // LOGI("return %d %d %d %d in i2c read jni", bufByte[0], bufByte[1], bufByte[2], bufByte[3]);
            __android_log_print(ANDROID_LOG_INFO, "IIC-SERVICE-JNI", "return %d %d %d %d in i2c read jni", bufByte[0], bufByte[1], bufByte[2], bufByte[3]);

            env->SetIntArrayRegion( bufArr, 0, len, bufInt);
        }

        free(bufByte);
        free(bufInt);
        
        return j;
        
        // err2:
        // free(bufByte);
        // err1:
        // free(bufInt);
        // err0:
       // return -1;                                          
    }

    //***************************************************************************
    // Write data to the I2C device
    //***************************************************************************
  
    static jint  writeI2C(JNIEnv *env, jobject obj, jint fileHander, 
                                           jint slaveAddr, jint regAddr, jint bufArr, jint len)
    {
        // jint *bufInt;
        // char *bufByte;
        int res = 0;
        
        if (len <= 0) {
            // LOGE("I2C: buf len <=0");
            __android_log_print(ANDROID_LOG_ERROR, "IIC-SERVICE-JNI", "%s","buf len <=0");

            return -1;
        }
        
        // bufInt = (jint *) malloc(len * sizeof(int));
        // if (bufInt == 0) {
        //     // LOGE("I2C: nomem");
        //     __android_log_print(ANDROID_LOG_ERROR, "I2C-HAL", "%s","nomem");

        //     return -1;
        // }
        // bufByte = (char*) malloc(len + 1);
        // if (bufByte == 0) {
        //     // LOGE("I2C: nomem");
        //     __android_log_print(ANDROID_LOG_ERROR, "I2C-HAL", "%s","nomem");

        //     free(bufInt);
        //     return -1;     
        // }
        
        // env->GetIntArrayRegion( bufArr, 0, len, bufInt);
        // bufByte[0] = mode;
        // for (i = 0; i < len; i++)
        //     bufByte[i] = bufInt[i];      
        
        
        // res = ioctl(fileHander, I2C_SLAVE, slaveAddr);
        // // if (res != 0) {
        //  if (res < 0) {    
        //     // LOGE("I2C: Can't set slave address");
        //     __android_log_print(ANDROID_LOG_ERROR, "I2C-HAL", "%s error :%d","Can't set slave address..",res);
        //     free(bufByte);
        //     free(bufInt);
        //     return -1;     
        // }
        

        res=iic_device->iic_write(iic_device,(int)fileHander,slaveAddr,regAddr,bufArr);
          if (res < 0) {    
            // LOGE("I2C: Can't set slave address");
            __android_log_print(ANDROID_LOG_ERROR, "IIC-SERVICE-JNI", "%s error :%d","Can't set slave address..",res);

            return -1;     
        }

        // if ((j = write(fileHander, bufByte, len)) != len) {
        //     // LOGE("write fail in i2c");
        //     __android_log_print(ANDROID_LOG_ERROR, "I2C-HAL", "%s","write fail in i2c");
        //     free(bufByte);
        //     free(bufInt);
        //     return -1;     
        // }
        
        // LOGI("I2C: write %d byte (%02x)", j, bufByte[0]);
        // __android_log_print(ANDROID_LOG_INFO, "IIC-SERVICE-JNI", "write %d byte (%02x)", j, bufByte[0]);

        // free(bufByte);
        // free(bufInt);
        
        return 1;
    }
  
    //***************************************************************************
    // Close the I2C device
    //***************************************************************************

    static void  closeI2C(JNIEnv *env, jobject obj, jint fileHander)
    {
        close(fileHander);
    }



        /*JNI Method Table*/
    static const JNINativeMethod method_table[] = {
        {"open_i2c_native"  ,  "(Ljava/lang/String;)I",  (void*)openI2C},
        {"read_i2c_native"  ,  "(III[II)I",               (void*)readI2C},
        {"write_i2c_native" ,  "(IIIII)I",               (void*)writeI2C},
        {"close_i2c_native" ,  "(I)V",                    (void*)closeI2C},
    };



    /*Register JNI method*/
    int register_android_server_IICService(JNIEnv *env) {    
            return jniRegisterNativeMethods(env, "com/android/server/nymbleperipheral/NymbleI2CService", method_table, NELEM(method_table));
        //    com/android/server/nymbleperipheral/NymbleI2CService"
    }
};
