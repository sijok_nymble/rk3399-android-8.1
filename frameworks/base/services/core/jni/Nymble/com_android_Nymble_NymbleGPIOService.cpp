/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define LOG_TAG "GPIOServiceJNI"
#include "utils/Log.h"

#include "jni.h"
#include <nativehelper/JNIHelp.h>
#include "android_runtime/AndroidRuntime.h"


#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <unistd.h> 
#include <errno.h>


namespace android
    {

    static int  
    nymble_gpio_setstate(JNIEnv *env, jobject thiz,jint mcunumber, jint state)
    {
    
        __android_log_print(ANDROID_LOG_DEBUG, "NYMBLE GPIO-SERVICE-JNI", "%s", " dev open call1... ");
    
        char fileName[64];//="/sys/ngpio/gpio32/modecb";
        char bufByte[10];//="on";
      
        int j, len;
        int fd;             // File handle
        
        memset(fileName, 0, 64);
        switch(mcunumber)
        {
            case 7:
                sprintf(fileName, "/sys/ngpio/gpio%d/mode7", 32);
                __android_log_print(ANDROID_LOG_DEBUG, "NYMBLE GPIO-SERVICE-JNI", "%s", "mcunumber 7");
               break;
            case 11:
                sprintf(fileName, "/sys/ngpio/gpio%d/mode11", 33);
                __android_log_print(ANDROID_LOG_DEBUG, "NYMBLE GPIO-SERVICE-JNI", "%s", "mcunumber 11");
               break;
            case 13:
                sprintf(fileName, "/sys/ngpio/gpio%d/mode13", 35);
                __android_log_print(ANDROID_LOG_DEBUG, "NYMBLE GPIO-SERVICE-JNI", "%s", "mcunumber 13");
               break;
            case 15:
                sprintf(fileName, "/sys/ngpio/gpio%d/mode15", 36);
                __android_log_print(ANDROID_LOG_DEBUG, "NYMBLE GPIO-SERVICE-JNI", "%s", "mcunumber 15");
               break;
            case 16:
                sprintf(fileName, "/sys/ngpio/gpio%d/mode16", 54);
                __android_log_print(ANDROID_LOG_DEBUG, "NYMBLE GPIO-SERVICE-JNI", "%s", "mcunumber 16");
               break;
            case 18:
                sprintf(fileName, "/sys/ngpio/gpio%d/mode18", 55);
                __android_log_print(ANDROID_LOG_DEBUG, "NYMBLE GPIO-SERVICE-JNI", "%s", "mcunumber 18");
               break;
            case 22:              
                sprintf(fileName, "/sys/ngpio/gpio%d/mode22", 56);
                __android_log_print(ANDROID_LOG_DEBUG, "NYMBLE GPIO-SERVICE-JNI", "%s", "mcunumber 22");
               break;
            case 26:
                sprintf(fileName, "/sys/ngpio/gpio%d/mode26", 149);
                __android_log_print(ANDROID_LOG_DEBUG, "NYMBLE GPIO-SERVICE-JNI", "%s", "mcunumber 26");
               break;
            case 37:
                sprintf(fileName, "/sys/ngpio/gpio%d/mode37", 124);
                __android_log_print(ANDROID_LOG_DEBUG, "NYMBLE GPIO-SERVICE-JNI", "%s", "mcunumber 37");
               break;
            case 38:
                sprintf(fileName, "/sys/ngpio/gpio%d/mode38", 125);
                __android_log_print(ANDROID_LOG_DEBUG, "NYMBLE GPIO-SERVICE-JNI", "%s", "mcunumber 38");
               break;
            case 40:
                sprintf(fileName, "/sys/ngpio/gpio%d/mode40", 126);
                __android_log_print(ANDROID_LOG_DEBUG, "NYMBLE GPIO-SERVICE-JNI", "%s", "mcunumber 40");
               break;
            default:
                jniThrowException(env, "java/lang/IllegalArgumentException", "MCU number not valid ");
                break;
        }

          __android_log_print(ANDROID_LOG_DEBUG, "NYMBLE GPIO-SERVICE-JNI - gpio_Path", "%s", fileName);
     
        fd = open(fileName, O_RDWR); 

        if(fd == 0)                 // Failed to open?
        {
            __android_log_print(ANDROID_LOG_DEBUG, "NYMBLE GPIO-SERVICE-JNI", "%s", " dev open failed... ");

            return(-2);
        }

        if(state == 1)              // LOW
        {
            strcpy(bufByte, "off");
        }
        else if(state == 2)                       // HIGH
        {
            strcpy(bufByte, "on"); 

        }else if(state==3){

            strcpy(bufByte, "reset");

        }else {

            strcpy(bufByte, "off");
        }

        __android_log_print(ANDROID_LOG_DEBUG, "NYMBLE GPIO-SERVICE-JNI -comm", "%s", bufByte);



        len = strlen(bufByte);
        if ((j = write(fd, bufByte, len)) != len) 
        {
             
            __android_log_print(ANDROID_LOG_DEBUG, "NYMBLE GPIO-SERVICE-JNI -comm", "%s"," write fail in GPIO output setting");

        }
        close(fd);
        return 1;   // Successful
    }




    static const JNINativeMethod method_table[] = {
        {"native_gpio_setstate",           "(II)I",                                                (void *)nymble_gpio_setstate},
    };

    int register_android_Nymble_GPIOService(JNIEnv *env)
    {

        return jniRegisterNativeMethods(env, "com/android/server/nymbleperipheral/NymbleGPIOService",
                method_table, NELEM(method_table));
    }

};
