/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions an
 * limitations under the License.
 */
package com.android.server.nymbleperipheral;


import android.nymbleperipheral.INymbleI2CManager;
import android.content.Context;
import android.os.RemoteException;
import android.util.Slog;
import android.content.Context;
import com.android.server.SystemService;


public class NymbleI2CService extends INymbleI2CManager.Stub {    
    private static final String TAG = "NymbleI2cService";
    private final Context mContext;

   public NymbleI2CService(Context context) {  
        mContext = context;  
    }


    public int open(String pathName){
        return open_i2c_native(pathName);    
     }

    public int write(int fileHander,int slaveAddr,int regAddr, int buffdata, int len) {    
       return write_i2c_native(fileHander,slaveAddr,regAddr,buffdata,len);   
    }



    public int read(int fileHander,int slaveAddr,int regAddr, int[] buffArray, int len) {   
        return read_i2c_native(fileHander,slaveAddr,regAddr, buffArray,len);
    }
 
    public void close(int fileHander){
            close_i2c_native(fileHander);
    }

 
    private static native int open_i2c_native(String path);
    private static native int read_i2c_native(int fileHander,int slaveAddr,int regAddr,int[] bufArr,int len);
    private static native int write_i2c_native(int fileHander,int slaveAddr, int regAddr, int bufArr, int len);
    private static native void close_i2c_native(int fileHander);
};