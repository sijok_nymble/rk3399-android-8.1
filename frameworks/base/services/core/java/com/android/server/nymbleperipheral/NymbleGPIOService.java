/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions an
 * limitations under the License.
 */

package com.android.server.nymbleperipheral;

import android.content.Context;
import android.nymbleperipheral.INymbleGPIOManager;
import java.io.IOException;
import java.io.File;
import java.util.ArrayList;
import java.nio.ByteBuffer;

public class NymbleGPIOService extends INymbleGPIOManager.Stub {

    private final Context mContext;

    public NymbleGPIOService(Context context)
    {
        mContext = context;
    }


  public int setmcustate(int mcu ,int state){

    return native_gpio_setstate(mcu,state);
  }


  private native int native_gpio_setstate(int mcunumber,int mcustate);


 

}
