/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions an
 * limitations under the License.
 */

package com.android.server.nymbleperipheral;

import android.content.Context;
import android.hardware.ISerialManager;
import android.nymbleperipheral.INymblePeripheralManager;
import android.os.ParcelFileDescriptor;

import java.io.FileDescriptor;
import java.io.IOException;
import java.io.File;
import java.util.ArrayList;
import java.nio.ByteBuffer;

public class NymblePeripheralService extends INymblePeripheralManager.Stub {

    private final Context mContext;
    private final String[] mSerialPorts;

    // used by the JNI code
    private int mNativeContext;
    // private final String mName;
    private ParcelFileDescriptor mFileDescriptor;



    public NymblePeripheralService(Context context)
    {
        mContext = context;
        mSerialPorts = context.getResources().getStringArray(
                com.android.internal.R.array.config_serialPorts);
    }

    /**
   //  * Returns the name of the serial port
   //  *
   //  * @return the serial port's name
   //  */
  public String[] getSerialPorts()
   {
       ArrayList<String> ports = new ArrayList<String>();
       for (int i = 0; i < mSerialPorts.length; i++) {
           String path = mSerialPorts[i];
           if (new File(path).exists()) {
               ports.add(path);
           }
       }
       String[] result = new String[ports.size()];
       ports.toArray(result);
       return result;
   }

    /**
    * Returns the file descriptor of the i2c port
    * @param path to the node
    *
    * @return the Serial port fle descriptor
    */
  public ParcelFileDescriptor openSerialPort(String path) {
        for (int i = 0; i < mSerialPorts.length; i++) {
            if (mSerialPorts[i].equals(path)) {
                return serial_native_open(path);
            }
        }
        throw new IllegalArgumentException("Invalid serial port " + path);
    }

  public void portOpen(ParcelFileDescriptor pfd, int speed)
  {

        native_serial_open(pfd.getFileDescriptor(), speed);
        mFileDescriptor = pfd;
        //  throw new IllegalArgumentException("Invalid serial portOpen ");
    }


    /**
     * Closes the serial port
     *
     */
  public void uartclose()
  {
      try{
         if (mFileDescriptor != null) {
             mFileDescriptor.close();
             mFileDescriptor = null;
         }
         native_serial_close();
       }catch(IOException ex){
         throw new IllegalArgumentException("Invalid close port ");
       }
   }

     /**
      * Reads data into the provided buffer.
      *
      * unchanged after a call to this method.
      *
      * @param buffer to read into
      * @param length
      * @return number of bytes read
      */
  public int uartRead(byte[] buffer,int length)
  {


        return native_serial_read_array(buffer, length);



  }
     /**
      *
      Writes data from provided buffer.
      *
      * unchanged after a call to this method.
      *
      * @param buffer to write
      * @param length number of bytes to write
      */
  public void uartWrite(byte[] buffer, int length)
  {
       native_serial_write_array(buffer, length);
   }



  private native ParcelFileDescriptor serial_native_open(String path);
  private native void native_serial_open(FileDescriptor pfd, int speed);
  private native void native_serial_close();
  private native int native_serial_read_array(byte[] buffer, int length) ;
  private native int native_serial_read_direct(ByteBuffer buffer, int length) ;
  private native void native_serial_write_array(byte[] buffer, int length);
  private native void native_serial_write_direct(ByteBuffer buffer, int length);



}
