#include <utils/Log.h>
#include <hardware/hardware.h>
#include <hardware/hpta32x32.h>
#include <stdio.h>
#include <android/log.h>

#define HTPA32X32_CONTROLLER  "hpta"

  
static struct hpta_device_t* hpta_device;
static hw_module_t* module;
static hw_device_t* device;



  /* open the device*/
static int hpta_device_open(){
        int err;

  printf("native deviceOpen ...\n");
       // __android_log_print(ANDROID_LOG_ERROR, "HPTA-SERVICE-JNI", "%s", "1. native deviceOpen ... ");

        /* 1. hw_get_module */
        err = hw_get_module("hpta", (hw_module_t const**)&module);
       // __android_log_print(ANDROID_LOG_ERROR, "HPTA-SERVICE-JNI device Open error", "%d",err); 
    //printf("device Open error ..\n");
        if (err == 0) {
            /* 2. get device : module->methods->open */
            err = module->methods->open(module, HPTA_HARDWARE_MODULE_ID, &device);
         //  __android_log_print(ANDROID_LOG_ERROR, "HPTA-SERVICE-JNI", "%s", "2. get device ... ");
    printf("2. get device ... \n");
            if (err == 0) {
                /* 3. call dev_open */
           //     __android_log_print(ANDROID_LOG_ERROR, "HPTA-SERVICE-JNI", "%s", "3. call dev open ... ");
    printf("3. call dev open ... \n");
                hpta_device = (struct hpta_device_t *)device;
 
                return hpta_device->hpta_open(hpta_device);
            } else {
          //  __android_log_print(ANDROID_LOG_ERROR, "HPTA-SERVICE-JNI", "%s", "error dev open ... ");
    printf( "error dev open ... ");
                return -1;
            }
        }
        return -1;
    }
  /* calibrate reg*/
// static int hpta_cal(){
//  //__android_log_print(ANDROID_LOG_ERROR, "HPTA-TEST", "%s", "hpta call ");  
//  //int val = value;
//  //ALOGI(" JNI: set value %d to device.", val);
//  printf(" hpta calibrate call \n");

//  if(!hpta_device) {
//  //  __android_log_print(ANDROID_LOG_ERROR, "HPTA-TEST", "%s", "open device error ");  
//    return -1;
//  }
    
//  return  hpta_device->hpta_calibrate(hpta_device);
//    }

        
// static int hpta_getVal() {
//  int g_b[2580];
//  int *val ;
//  if(!hpta_device) {
//  printf("!hpta_device \n");
//      //__android_log_print(ANDROID_LOG_ERROR, "HPTA-TEST", "%s", "open device error ");
//   return -1;
//  }
//  printf(" NymbleTest hpta_device->hpta_get_val(hpta_device,g_b);...\n");
//  hpta_device->hpta_get_val(hpta_device,g_b);
    
//    printf("Alternate elements of a given array \n");
//    for (int i = 0; i < 2580; i++){
//       printf( " %d -\n", g_b[i]);
    
//    }
  
//  return 1;
//  }





static int test_hpta_calibrate(struct hpta_device_t* hpta_device) 
{

  //__android_log_print(ANDROID_LOG_ERROR, "HPTA-HAL", "%s", "device calibrate call ");


  unsigned char msg_addr_mbit[2] = { TRIM_REG1, MBIT_TRIM };

  struct i2c_msg msgscalib[1] = {
    { .addr = ADDR, 
      .flags = 0,
      .len = 2, 
      .buf = msg_addr_mbit 
    },

  };


  if(hpta_device->i2c_transfer(hpta_device, msgscalib, 1) < 0) {
    return false;
  }
  usleep(5000);
  //__android_log_print(ANDROID_LOG_ERROR, "HPTA-HAL Test", "%s-","TestS1");
  unsigned char msg_addr_bias1[2] = { TRIM_REG2, BIAS_TRIML };
  msgscalib->buf = msg_addr_bias1;
  if(hpta_device->i2c_transfer(hpta_device, msgscalib, 1) < 0) {
    return false;
  }
  usleep(5000);
  //__android_log_print(ANDROID_LOG_ERROR, "HPTA-HAL Test", "%s-","TestS2");
  unsigned char msg_addr_bias2[2] = { TRIM_REG3, BIAS_TRIMR };
  msgscalib->buf = msg_addr_bias2;
  if(hpta_device->i2c_transfer(hpta_device, msgscalib, 1) < 0) {
    return false;
  }
  usleep(5000);
  //__android_log_print(ANDROID_LOG_ERROR, "HPTA-HAL Test", "%s-","TestS3");
  unsigned char msg_addr_clk[2] = { TRIM_REG4, CLK_TRIM };
  msgscalib->buf = msg_addr_clk;
  if(hpta_device->i2c_transfer(hpta_device, msgscalib, 1) < 0) {
    return false;
  }
  usleep(5000);
  //__android_log_print(ANDROID_LOG_ERROR, "HPTA-HAL Test", "%s-","TestS4");
  unsigned char msg_addr_bpa1[2] = { TRIM_REG5, BPA_TRIML };
  msgscalib->buf = msg_addr_bpa1;
  if(hpta_device->i2c_transfer(hpta_device, msgscalib, 1) < 0) {
    return false;
  }
  usleep(5000);
  //__android_log_print(ANDROID_LOG_ERROR, "HPTA-HAL Test", "%s-","TestS5");
  unsigned char msg_addr_bpa2[2] = { TRIM_REG6, BPA_TRIMR };
  msgscalib->buf =msg_addr_bpa2;
  if(hpta_device->i2c_transfer(hpta_device, msgscalib, 1) < 0) {
    return false;
    }
  usleep(5000);
  //__android_log_print(ANDROID_LOG_ERROR, "HPTA-HAL Test", "%s-","TestS6");
  unsigned char msg_addr_pu[2] = { TRIM_REG7, PU_TRIM };
  msgscalib->buf = msg_addr_pu;
  if(hpta_device->i2c_transfer(hpta_device, msgscalib, 1) < 0) {
    return false;
  }

  //__android_log_print(ANDROID_LOG_ERROR, "HPTA-HAL Test", "%s-","TestS7");

  return true;
}


static int test_hpta_get_val(struct hpta_device_t* dev,int * g_b)
{
  /*if(!dev) {
    //LOGE("Hpta Stub: error val pointer");
    return -EFAULT;
  } */
  //int g_b[2580];
  //__android_log_print(ANDROID_LOG_ERROR, "HPTA-HAL Test", "%s-","Test1");

  int j = 0;
  unsigned char msg_addr_blk[2] = { CONFIG_REG, START_WAKEUP };     //starting the thermal reading
  struct i2c_msg msgs_blk[1] = {
    { .addr = ADDR, .flags = 0, .len = 2, .buf = msg_addr_blk },
  };
  if(hpta_device->i2c_transfer(hpta_device, msgs_blk, 1) < 0) {

        return false;
  }

  usleep(30000);

  unsigned char msg_addr_status[1] = { STATUS_REG };
  unsigned char msg_data_status[1] = {};
  struct i2c_msg msgs_status[2] = {
    { .addr = ADDR,
      .flags = 0,
      .len = 1,
      .buf = msg_addr_status },

    /** Read 8-bit data */
    { .addr = ADDR,
      .flags = I2C_M_RD,
      .len = 1,
      .buf = msg_data_status },
  };

  /** Transfer a transaction with two I2C messages */
  if(hpta_device->i2c_transfer(dev, msgs_status, 2) < 0) {
    return false;
  }

    /**Reading top block*//**Reading sensor values Block 0*/
  unsigned char msg_addr_read[1] = { READ_BLOCK_TOP_REG };
  unsigned char msg_data_read[258] = {};
  struct i2c_msg msgs_read[2] = {
    { .addr = ADDR, .flags = 0, .len = 1, .buf = msg_addr_read },
    { .addr = ADDR,
      .flags = I2C_M_RD,
      .len = 258,
      .buf = msg_data_read },
  };

  if(hpta_device->i2c_transfer(hpta_device, msgs_read, 2) < 0) {
    return false;
  }
  for (int i = 0; i < 258; i++, j++)
  {
    g_b[j] = msg_data_read[i];
    //__android_log_print(ANDROID_LOG_ERROR, "", "%d-",g_b[1]);

  }

    /**Rading bottom block*/
  msg_addr_read[0] = READ_BLOCK_BOTTOM_REG;
  /** Transfer a transaction with two I2C messages */
  if(hpta_device->i2c_transfer(hpta_device, msgs_read, 2) < 0) {
    return false;
  }
  for (int i = 0; i < 258; i++, j++){
    g_b[j] = msg_data_read[i];
  }
                  /**Reading sensor values Block 1*/
  msg_addr_blk[1] = BLOCK1_CONFIG;
  if(hpta_device->i2c_transfer(hpta_device, msgs_blk, 1) < 0) {
    return false;
  }
  usleep(30000);

  msg_addr_status[0] = STATUS_REG;
  /** Transfer a transaction with two I2C messages */
  if(hpta_device->i2c_transfer(hpta_device, msgs_status, 2) < 0) {
    return false;
  }

  /**Reading top block*/
  msg_addr_read[0] = READ_BLOCK_TOP_REG;
  if(hpta_device->i2c_transfer(hpta_device, msgs_read, 2) < 0) {
    return false;
  }

  for (int i = 0; i < 258; i++, j++){
    g_b[j] = msg_data_read[i];
  }

  /**Rading bottom block*/
  msg_addr_read[0] = READ_BLOCK_BOTTOM_REG;
  /** Transfer a transaction with two I2C messages */
  if(hpta_device->i2c_transfer(hpta_device, msgs_read, 2) < 0) {
    return false;
  }
  for (int i = 0; i < 258; i++, j++){
    g_b[j] = msg_data_read[i];
  }
                  /**Reading sensor values Block 2*/
  msg_addr_blk[1] = BLOCK2_CONFIG;
  if(hpta_device->i2c_transfer(hpta_device, msgs_blk, 1) < 0) {
    return false;
  }
  usleep(30000);

  msg_addr_status[0] = STATUS_REG;
  /** Transfer a transaction with two I2C messages */
  if(hpta_device->i2c_transfer(hpta_device, msgs_status, 2) < 0) {
    return false;
  }

  /**Reading top block*/
  msg_addr_read[0] = READ_BLOCK_TOP_REG;
  if(hpta_device->i2c_transfer(hpta_device, msgs_read, 2) < 0) {
    return false;
  }
  for (int i = 0; i < 258; i++, j++){
    g_b[j] = msg_data_read[i];
  }
  /**Rading bottom block*/
  msg_addr_read[0] = READ_BLOCK_BOTTOM_REG;
  /** Transfer a transaction with two I2C messages */
  if(hpta_device->i2c_transfer(hpta_device, msgs_read, 2) < 0) {
    return false;
  }
  for (int i = 0; i < 258; i++, j++){
    g_b[j] = msg_data_read[i];
  }
                  /**Reading sensor values Block 3*/
  msg_addr_blk[1] = BLOCK3_CONFIG;
  if(hpta_device->i2c_transfer(hpta_device, msgs_blk, 1) < 0) {
    return false;
  }
  usleep(30000);

  msg_addr_status[0] = STATUS_REG;
  /** Transfer a transaction with two I2C messages */
  if(hpta_device->i2c_transfer(hpta_device, msgs_status, 2) < 0) {
    return false;
    }

  /**Reading top block*/
  msg_addr_read[0] = READ_BLOCK_TOP_REG;
  if(hpta_device->i2c_transfer(hpta_device, msgs_read, 2) < 0) {
    return false;
  }

  for (int i = 0; i < 258; i++, j++){
    g_b[j] = msg_data_read[i];
  }
  /**Rading bottom block*/
  msg_addr_read[0] = READ_BLOCK_BOTTOM_REG;
  /** Transfer a transaction with two I2C messages */
  if(hpta_device->i2c_transfer(hpta_device, msgs_read, 2) < 0) {
    return false;
  }

  for (int i = 0; i < 258; i++, j++){
    g_b[j] = msg_data_read[i];
  }


  /**Electrical Offsets*/
  msg_addr_blk[1] = EL_OFFSETS_TOP_REG;
  if(hpta_device->i2c_transfer(hpta_device, msgs_blk, 1) < 0) {
    return false;
  }
  usleep(30000);

  msg_addr_status[0] = STATUS_REG;
  /** Transfer a transaction with two I2C messages */
  if(hpta_device->i2c_transfer(hpta_device, msgs_status, 2) < 0) {
    return false;
  }

  /**Reading top block*/
  msg_addr_read[0] = READ_BLOCK_TOP_REG;
  if(hpta_device->i2c_transfer(hpta_device, msgs_read, 2) < 0) {
    return false;
  }
  for (int i = 0; i < 258; i++, j++)
  {
    g_b[j] = msg_data_read[i];
  }
  /**Rading bottom block*/
  msg_addr_read[0] = READ_BLOCK_BOTTOM_REG;
  /** Transfer a transaction with two I2C messages */
  if(hpta_device->i2c_transfer(hpta_device, msgs_read, 2) < 0) {
    return false;
  }

  for (int i = 0; i < 258; i++, j++){
    g_b[j] = msg_data_read[i];
  }


   //__android_log_print(ANDROID_LOG_ERROR, "", "%d-",g_b[155]);


   return 1;
}


int main(){

  printf(" NymbleTest main...\n");
  int g_b[2580];
  if(hpta_device_open()) {
  printf(" NymbleI2C Open succes...\n");
   }else return 0;




  if(test_hpta_calibrate(hpta_device)){
      printf(" NymbleTest test_hpta_calibrate succes...\n");
    }else{printf(" NymbleTest test_hpta_calibrate error...\n");
      return 0;
    }

  if (test_hpta_get_val(hpta_device,g_b))
  {
   
      printf(" NymbleI2C g_b succes...\n");
 
  }else{printf(" NymbleTest g_b error...\n");
      return 0;
    }

  for (int i = 0; i < 2580; i++){
         printf( " %d -\n", g_b[i]);
      
      }
  //hpta_device->common.close(device);

}
