#include <utils/Log.h>
#include <hardware/hardware.h>
#include <hardware/service_uart.h>
#include <stdio.h>
#include <android/log.h>


	
static struct uart_device_t* uart_device;
static hw_module_t* module;
static hw_device_t* device;



	/* open the device*/
static int uarttest_device_open(){
        int err;

    		printf("native deviceOpen ...\n");
       // __android_log_print(ANDROID_LOG_ERROR, "HPTA-SERVICE-JNI", "%s", "1. native deviceOpen ... ");

        /* 1. hw_get_module */
        err = hw_get_module("serviceuart", (hw_module_t const**)&module);
       // __android_log_print(ANDROID_LOG_ERROR, "HPTA-SERVICE-JNI device Open error", "%d",err); 
		//printf("device Open error ..\n");
        if (err == 0) {
            /* 2. get device : module->methods->open */

            err = module->methods->open(module,UART_HARDWARE_MODULE_ID, &device);
         //  __android_log_print(ANDROID_LOG_ERROR, "HPTA-SERVICE-JNI", "%s", "2. get device ... ");
		        printf("2. get device ... \n");
            if (err == 0) {
                /* 3. call dev_open */
           //     __android_log_print(ANDROID_LOG_ERROR, "HPTA-SERVICE-JNI", "%s", "3. call dev open ... ");
		        printf("3. call dev open ... \n");
                uart_device = (struct uart_device_t *)device;
 
                return uart_device->uart_open(uart_device,"/dev/ttyS4",9600);
            } else {
          //  __android_log_print(ANDROID_LOG_ERROR, "HPTA-SERVICE-JNI", "%s", "error dev open ... ");
		            printf( "error dev open ... ");
                return -1;
            }
        }
        return -1;
    }

	
static int datawrte(struct uart_device_t* uart_device,char * buffer,int length)
{
  return uart_device->uart_write(uart_device,buffer,length);
}

static int dataread(struct uart_device_t* uart_device,char * buffer,int length)
{
  return uart_device->uart_read(uart_device,buffer,length);
}



int main(){

	printf(" NymbleTest main...\n");
	
	int fd=uarttest_device_open();
	printf(" Nymble Uart FD %d...\n",fd);
	if(fd) {
	printf(" NymbleTest 12...\n");
	 }else return 0;

char  buff[4]="ACK";

int ret=datawrte(uart_device,buff,4);

if(ret<0)printf("Error Writing :ret is ..%d\n",ret );


char buff1[10]={0};

while(1){
  usleep(100000);
int ret1=dataread(uart_device,buff1,sizeof(buff1));
usleep(10000);
if(ret1<0)printf("Error Reading :ret1 is ..%d\n",ret1 );
}
}