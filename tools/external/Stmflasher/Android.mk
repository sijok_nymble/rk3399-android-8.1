# LOCAL_PATH:= $(call my-dir)

# include $(call all-named-subdir-makefiles, parsers)
# include $(CLEAR_VARS)
# #LOCAL_ALLOW_UNDEFINED_SYMBOLS := true
# LOCAL_SRC_FILES := \
# 	parsers\ \

# LOCAL_MODULE := stm-flasher
# LOCAL_MODULE_TAGS := debug
# #LOCAL_SHARED_LIBRARIES := liblog 
# LOCAL_LDLIBS := -Llibs \
#         -lcutils -lhardware
# include $(BUILD_EXECUTABLE)





TOP_LOCAL_PATH := $(call my-dir)

include $(call all-named-subdir-makefiles, parsers)

LOCAL_PATH := $(TOP_LOCAL_PATH)

include $(CLEAR_VARS)
LOCAL_MODULE := stm32flash
LOCAL_SRC_FILES :=	\
	dev_table.c	\
	flasher.c   \
	i2c.c		\
	init.c		\
	port.c		\
	serial_common.c	\
	serial_posix.c	\
	stm32.c		\
	utils.c
LOCAL_STATIC_LIBRARIES := libparsers
include $(BUILD_EXECUTABLE)
