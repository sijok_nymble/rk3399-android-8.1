LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)
#LOCAL_ALLOW_UNDEFINED_SYMBOLS := true
LOCAL_SRC_FILES := \
	nymble.c \

LOCAL_MODULE := nymbleuart
LOCAL_MODULE_TAGS := debug
#LOCAL_SHARED_LIBRARIES := liblog 
LOCAL_LDLIBS := -Llibs \
        -lcutils -lhardware
include $(BUILD_EXECUTABLE)
